﻿using System.Threading.Tasks;

namespace VTS.RouteAuditor.ShipmentValidator
{
    public interface IWorkItem
    {
        Task RunAsync();
    }
}