﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Models;

namespace VTS.RouteAuditor.ShipmentValidator
{
    public class WorkItem : IWorkItem
    {
        private readonly ILogger<WorkItem> _log;
        private readonly IConfiguration _config;
        private readonly IParseData<RoutePlan> _routeParser;
        private readonly IParseData<IEnumerable<IShipment>> _shipmentParser;

        public WorkItem(ILogger<WorkItem> log, IConfiguration config, IParseData<RoutePlan> routeParser, IParseData<IEnumerable<IShipment>> shipmentParser)
        {
            _log = log;
            _config = config;
            _routeParser = routeParser;
            _shipmentParser = shipmentParser;
        }
        public async Task RunAsync()
        {
            var path = @"E:\AppDev\VTS.RouteAuditor.Solution\Documentation\Liberty Coca-Cola Dev";
            var files = Directory.EnumerateFiles(path);
            string fileName = string.Empty;

            var routePlans = new List<RoutePlan>();
            var shipments = new List<IShipment>();

            foreach (var file in files)
            {
                fileName = Path.GetFileName(file);

                if (await _routeParser.CanParseAsync(fileName))
                {
                    var routePlan = await ValidateRoutePlanAsync(file);
                    if (routePlan != default)
                    {
                        //var waitTime = routePlan.WaitTime;
                        //var driveTime = routePlan.DriveTime;
                        routePlans.Add(routePlan);
                    }
                }
                else if (await _shipmentParser.CanParseAsync(fileName))
                {
                    var shipment = await ValidateShipmentAsync(file);
                    if (shipment != default)
                        shipments.AddRange(shipment);
                }
            }

            foreach (var shipment in shipments)
            {
                var routePlan = routePlans.FirstOrDefault(r => r.RouteIdentifier.Equals(shipment.RouteIdentifier));
                var tour = routePlan?.Tours.FirstOrDefault();

                if (tour is not null)
                {
                    _log.LogInformation($"Route with TourID [{tour.TourIdentifier}] is assigned to Driver [{shipment.DriverIdentifier}]/Vehicle [{shipment.VehicleIdentifier}]");
                }
            }
        }

        private async Task<RoutePlan> ValidateRoutePlanAsync(string file)
        {
            _log.LogInformation($"Validating route plan from {file}");

            var data = await File.ReadAllBytesAsync(file);
            try
            {
                RoutePlan routePlan = await _routeParser.ParseAsync(Path.GetFileName(file), data);
                routePlan.SourceFile = file;
                var tour = routePlan.Tours.FirstOrDefault();

                _log.LogInformation($"Successfully parsed route plan from {Path.GetFileName(file)} with TourID [{tour?.TourIdentifier}]");

                return routePlan;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = $"{msg}; {ex.InnerException.Message}";
                }
                _log.LogError($"Unhandled exception while parsing {file}: {msg}");
            }

            return default;
        }
        private async Task<IEnumerable<IShipment>> ValidateShipmentAsync(string file)
        {
            _log.LogInformation($"Validating shipment from {file}");

            var data = await File.ReadAllBytesAsync(file);
            try
            {
                var shipments = await _shipmentParser.ParseAsync(Path.GetFileName(file), data);
                _log.LogInformation($"Successfully parsed shipment from {file}");

                return shipments;
            }
            catch (Exception ex)
            {
                var msg = ex.Message;
                if (ex.InnerException != null)
                {
                    msg = $"{msg}; {ex.InnerException.Message}";
                }
                _log.LogError($"Unhandled exception while parsing {file}: {msg}");
            }

            return default;
        }
    }
}
