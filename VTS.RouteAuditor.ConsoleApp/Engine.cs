﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Services;

namespace VTS.RouteAuditor.ConsoleApp
{
    internal class Engine(IIngressService ingressService, ILogger<Engine> logger) : BackgroundService, IIngressServiceDelegate
    {
        private readonly IIngressService _ingressService = ingressService;
        private readonly ILogger<Engine> _logger = logger;

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            await Task.Yield();

            _logger.LogInformation("Starting engine");

            var currentPath = System.IO.Directory.GetCurrentDirectory();
            var mobileGroupId = 1;
            var options = new SFTPIngressOptions("r74vwolqoy7sq.eastus.azurecontainer.io", "ftpuser", "he3Wn@Et6q*JywzEhTxi%Xsas", remotePath: "/upload/development", localPath: $@"E:\Temp\CONADownloads\{mobileGroupId}");

            await _ingressService.InitializeAsync(options, this);

            //await _ingressService.StartAsync(stoppingToken);
            if (_ingressService is SFTPIngressService sftpIngress)
            {
                var files = sftpIngress.ListAllFiles(options.RemotePath);
                var downloadedFiles = sftpIngress.DownloadFiles(files);

                sftpIngress.ArchiveFiles(downloadedFiles.Select(f => f.RemotePath));
            }

            await Task.Delay(Timeout.Infinite, stoppingToken);
        }

        public Task<bool> ConsumeDataAsync(string dataIdentifier, byte[] data)
        {
            throw new NotImplementedException();
        }
    }
}
