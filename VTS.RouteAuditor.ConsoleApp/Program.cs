﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Services;
using VTS.RouteAuditor.ConsoleApp;

var host = Host.CreateApplicationBuilder(args);

host.Services.AddSerilog((sp, config) => config.ReadFrom.Configuration(host.Configuration));

host.Services.AddTransient<IIngressService, SFTPIngressService>();
host.Services.AddHostedService<Engine>();

await host.Build().RunAsync();
