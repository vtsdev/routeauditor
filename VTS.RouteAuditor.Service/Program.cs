﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Serilog;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace VTS.RouteAuditor.Service
{
    class Program
    {
        static async Task Main(string[] args)
        {
            try
            {
                var host = Host.CreateDefaultBuilder()
                    .ConfigureServices(ConfigureServices)
                    .UseSerilog();

#if DEBUG
                    host.UseEnvironment("Development");
#endif
                await host.RunConsoleAsync(opt => opt.SuppressStatusMessages = true);
            }
            catch (Exception ex)
            {
                Log.Logger.Error(ex, "Unhandled exception");
            }
        }

        static void ConfigureServices(HostBuilderContext context, IServiceCollection services)
        {
            var config = context.Configuration;

            Log.Logger = new LoggerConfiguration()
                .ReadFrom.Configuration(config)
                .CreateLogger();

            var env = context.HostingEnvironment;

            Log.Logger.Information($"Application is starting in {env.EnvironmentName} mode");

            DependencyContainer.Configure(services, config);
        }
    }
}
