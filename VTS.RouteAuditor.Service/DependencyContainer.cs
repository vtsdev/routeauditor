﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VTS.RouteAuditor.Common.Services;
using VTS.RouteAuditor.Core;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Service
{
    internal static class DependencyContainer
    {
        public static IServiceCollection Configure(IServiceCollection services, IConfiguration config)
        {
            services.LoadInjectableServices(type => type.Namespace?.StartsWith("VTS.RouteAuditor") ?? false);
            services.AddHostedService<Engine>();

            return services;
        }
    }
}
