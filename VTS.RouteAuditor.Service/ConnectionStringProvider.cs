﻿using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Text;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Service
{
    [Injectable(InjectableLifecycle.Singleton)]
    internal class ConnectionStringProvider : IConnectionStringProvider
    {
        private readonly IConfiguration _config;

        public ConnectionStringProvider(IConfiguration config)
        {
            _config = config;
        }
        public string GetConnectionString(string name)
        {
            return _config.GetConnectionString(name);
        }
    }
}
