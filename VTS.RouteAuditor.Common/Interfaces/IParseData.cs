﻿using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Models;

namespace VTS.RouteAuditor.Common.Interfaces
{
    public interface IParseData<TResult> where TResult : class
    {
        Task<bool> CanParseAsync(string dataIdentifier, byte[] data = null);
        Task<TResult> ParseAsync(string dataIdentifier, byte[] data);
    }
}
