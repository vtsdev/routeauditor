﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTS.RouteAuditor.Common.Interfaces
{
    public interface IPlannedStop
    {
        long Id { get; set; }
        long RoutePlanId { get; set; }
        string TaskId { get; }
        string CustomerNumber { get; }
        DateTime ArrivalTime { get; }
        DateTime DepartureTime { get; }
        int Cases { get; }
        int Cubes { get; }
    }
}
