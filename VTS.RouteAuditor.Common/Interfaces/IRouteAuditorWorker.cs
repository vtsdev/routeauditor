﻿using System;
using System.Threading;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Models;

namespace VTS.RouteAuditor.Common.Interfaces
{
    public interface IRouteAuditorWorker
    {
        Task RunAsyncAsync(Account account, CancellationToken cancellationToken);
    }
}
