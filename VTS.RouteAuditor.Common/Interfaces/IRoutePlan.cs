﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTS.RouteAuditor.Common.Interfaces
{
    public interface IRoutePlan
    {
        long Id { get; set; }
        string RouteIdentifier { get; }
        DateTime RouteDate { get; }
        string SourceFile { get; set; }
        string TourId { get; }
        DateTime StartDateTime { get; }
        DateTime EndDateTime { get; }
        TimeSpan DriveTime { get; }
        TimeSpan WaitTime { get; }
        double Distance { get; }

        IEnumerable<IPlannedStop> PlannedStops { get; }
    }
}
