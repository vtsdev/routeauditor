﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Models;

namespace VTS.RouteAuditor.Common.Interfaces
{
    public interface IIngressService
    {
        Task InitializeAsync(IIngressServiceOptions options, IIngressServiceDelegate ingressServiceDelegate);
        Task StartAsync(CancellationToken cancellationToken);
    }
}
