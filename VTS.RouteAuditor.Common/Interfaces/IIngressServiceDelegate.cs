﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace VTS.RouteAuditor.Common.Interfaces
{
    public interface IIngressServiceDelegate
    {
        Task<bool> ConsumeDataAsync(string dataIdentifier, byte[] data);
    }
}
