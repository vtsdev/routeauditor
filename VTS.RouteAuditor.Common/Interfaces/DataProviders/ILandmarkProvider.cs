﻿using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Models;

namespace VTS.RouteAuditor.Common.Interfaces.DataProviders
{
    public interface ILandmarkProvider
    {
        Task<Landmark> GetLandmarkByCustomerNumberAsync(int accountId, string customerNumber);
    }
}
