﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Models;

namespace VTS.RouteAuditor.Common.Interfaces.DataProviders
{
    public interface ITourPlanProvider
    {
        Task<TourPlan> GetByIdentifierAsync(int accountId, string tourIdentifier, int? tourSequence = null);
        Task<IEnumerable<TourPlan>> GetListAsync(int accountId, long? routePlanId);
        Task<TourPlan> InsertAsync(TourPlan item);
    }
}
