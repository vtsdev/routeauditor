﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Models;

namespace VTS.RouteAuditor.Common.Interfaces.DataProviders
{
    public interface IDriverProvider
    {
        Task<Driver> GetByDriverIdentifier(int accountId, string driverIdentifier);
    }
}
