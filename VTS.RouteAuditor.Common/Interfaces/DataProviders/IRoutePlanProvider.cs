﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Models;

namespace VTS.RouteAuditor.Common.Interfaces.DataProviders
{
    public interface IRoutePlanProvider
    {
        Task<RoutePlan> GetAsync(long id);
        Task<RoutePlan> GetByIdentifierAsync(int accountId, string routeIdentifier, DateTime routeDate);
        Task<RoutePlan> InsertAsync(RoutePlan item);
        Task<RoutePlan> UpdateAsync(RoutePlan item);
    }
}
