﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace VTS.RouteAuditor.Common.Interfaces
{
    public interface IShipment
    {
        string ShipmentIdentifier { get; }
        DateTime RouteDate { get; }
        string RouteIdentifier { get; }
        string TourIdentifier { get; }
        string DriverIdentifier { get; }
        string VehicleIdentifier { get; }
    }
}
