﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTS.RouteAuditor.Common.Interfaces
{
    public interface IConnectionStringProvider
    {
        string GetConnectionString(string name);
    }
}
