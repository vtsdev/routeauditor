﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VTS.RouteAutitor.Common.DependencyInjection
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InjectableAttribute : Attribute
    {
        public InjectableAttribute(InjectableLifecycle lifecycle = InjectableLifecycle.Transient)
        {
            Lifecycle = lifecycle;
        }

        public InjectableLifecycle Lifecycle { get; }
    }

    public enum InjectableLifecycle
    {
        Singleton,
        Scoped,
        Transient
    }

    internal struct InjectableInfo
    {
        internal InjectableInfo(Type implementation, IEnumerable<Type> services, InjectableAttribute injectableAttribute)
        {
            Services = services.ToArray();
            Implementation = implementation;
            Lifecycle = injectableAttribute?.Lifecycle ?? InjectableLifecycle.Transient;

            if (Services.Any(s => !s.IsInterface))
                throw new Exception($"Services must all be interfaces. (Implementation: {Implementation})");
        }

        /// <summary>
        /// Implementation of a service.
        /// </summary>
        public Type Implementation { get; }

        /// <summary>
        /// Interfaces that a service should implement. These should all be interfaces.
        /// </summary>
        public Type[] Services { get; }

        public InjectableLifecycle Lifecycle { get; }
    }
}
