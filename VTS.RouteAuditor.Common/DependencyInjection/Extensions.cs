﻿using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;

namespace VTS.RouteAutitor.Common.DependencyInjection
{
    public static class Extensions
    {
        public static IServiceCollection LoadInjectableServices(this IServiceCollection services, Func<Type, bool> typeFilter = null, params Type[] types)
        {
            GetInjectableServices(typeFilter, types)
                .ForEach(injectable => services.RegisterInjectable(injectable));

            return services;
        }

        private static List<InjectableInfo> GetInjectableServices(Func<Type, bool> typeFilter = null, params Type[] types)
        {
            Assembly assembly = default;
            IEnumerable<Type> assemblyTypes = default;
            IEnumerable<InjectableInfo> injectables = default;

            var retVal = new List<InjectableInfo>();
            var files = Directory.GetFiles(AppDomain.CurrentDomain.BaseDirectory, "VTS.*.dll", SearchOption.AllDirectories);

            foreach (string assemblyPath in files)
            {
                assembly = System.Runtime.Loader.AssemblyLoadContext.Default.LoadFromAssemblyPath(assemblyPath);
                if (assembly != null)
                {
                    assemblyTypes = assembly?.GetTypes().Where(t => !t.IsAbstract && !t.IsInterface);
                    if (typeFilter != null)
                        assemblyTypes = assemblyTypes?.Where(typeFilter);

                    injectables = assemblyTypes?
                        .Where(t => (types?.Any(i => i.IsAssignableFrom(t)) ?? false) || t.GetCustomAttributes<InjectableAttribute>().Any())
                        .Select(t => new InjectableInfo(
                            t,
                            t.GetInterfaces().Where(i => i != typeof(IDisposable)),
                            t.GetCustomAttributes<InjectableAttribute>().FirstOrDefault()));

                    if (injectables?.Count() > 0)
                        retVal.AddRange(injectables);
                }
            }

            return retVal;
        }
        private static void RegisterInjectable(this IServiceCollection services, InjectableInfo injectable)
        {
            switch (injectable.Lifecycle)
            {
                case InjectableLifecycle.Singleton:
                    services.RegisterSingletonInjectable(injectable);
                    break;
                case InjectableLifecycle.Scoped:
                    services.RegisterScopedInjectable(injectable);
                    break;
                default:
                    services.RegisterTransientInjectable(injectable);
                    break;
            };
        }

        private static void RegisterSingletonInjectable(this IServiceCollection services, InjectableInfo injectable)
        {
            services.AddSingleton(injectable.Implementation);
            foreach (var service in injectable.Services)
                services.AddSingleton(service, injectable.Implementation);
        }

        private static void RegisterScopedInjectable(this IServiceCollection services, InjectableInfo injectable)
        {
            services.AddScoped(injectable.Implementation);
            foreach (var service in injectable.Services)
                services.AddScoped(service, injectable.Implementation);
        }

        private static void RegisterTransientInjectable(this IServiceCollection services, InjectableInfo injectable)
        {
            services.AddTransient(injectable.Implementation);
            foreach (var service in injectable.Services)
                services.AddTransient(service, injectable.Implementation);
        }
    }
}
