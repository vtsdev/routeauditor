﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTS.RouteAuditor.Common.Models
{
    public class PlannedStop
    {
        public long Id { get; set; }
        public long TourPlanId { get; set; }
        public string TaskId { get; set; }
        public string Name { get; set; }
        public int StopNumber { get; set; }
        public string CustomerNumber { get; set; }
        public int? LandmarkId { get; set; }
        public TimeSpan? ArrivalTime { get; set; }
        public TimeSpan? DepartureTime { get; set; }
        public TimeSpan? WorkTime { get; set; }
        public int? Cases { get; set; }
        public int? Cubes { get; set; }
    }
}
