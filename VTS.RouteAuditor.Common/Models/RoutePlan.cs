﻿using System;
using System.Collections.Generic;
using System.Text;
using VTS.RouteAuditor.Common.Interfaces;

namespace VTS.RouteAuditor.Common.Models
{
    public class RoutePlan
    {
        public long Id { get; set; }
        public int AccountId { get; set; }
        public string RouteIdentifier { get; set; }
        public DateTime RouteDate { get; set; }
        public string SourceFile { get; set; }

        public IEnumerable<TourPlan> Tours { get; set; }
    }
}
