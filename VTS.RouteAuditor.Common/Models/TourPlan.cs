﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTS.RouteAuditor.Common.Models
{
    public class TourPlan
    {
        public long Id { get; set; }
        public long RoutePlanId { get; set; }
        public string TourIdentifier { get; set; }
        public int TourSequence { get; set; }
        public string ShipmentNumber { get; set; }
        public TimeSpan StartTime { get; set; }
        public TimeSpan EndTime { get; set; }
        public TimeSpan DriveTime { get; set; }
        public TimeSpan WaitTime { get; set; }
        public TimeSpan WorkTime { get; set; }
        public decimal Distance { get; set; }
        public string SourceFile { get; set; }
        public bool IsActive { get; set; }
        public string StatusMessage { get; set; }
        public DateTimeOffset CreatedOn { get; set; }

        public IEnumerable<PlannedStop> PlannedStops { get; set; }
    }
}
