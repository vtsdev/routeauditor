﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTS.RouteAuditor.Common.Models
{
    public class Account
    {
        public int Id { get; set; }
        public int MobileGroupId { get; set; }
        public string WorkerType { get; set; }
        public string SFTPHost { get; set; }
        public string SFTPUsername { get; set; }
        public string SFTPPassword { get; set; }
        public string SFTPRootPath { get; set; }
    }
}
