﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTS.RouteAuditor.Common.Models
{
    public class Landmark
    {
        public int Id { get; set; }
        public int MobileGroupId { get; set; }
        public int CategoryId { get; set; }
        public string LocationName { get; set; }
        public string Street { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public decimal Latitude { get; set; }
        public decimal Longitude { get; set; }
        public string Notes { get; set; }
        public int RadiusId { get; set; }
        public string AccountNo { get; set; }
        public int? TankNo { get; set; }
        public int? SubgroupId { get; set; }
    }
}
