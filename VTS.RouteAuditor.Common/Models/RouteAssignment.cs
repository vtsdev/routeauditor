﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTS.RouteAuditor.Common.Models
{
    public class RouteAssignment
    {
        public long Id { get; set; }
        public int AccountId { get; set; }
        public long? RoutePlanId { get; set; }
        public long? TourPlanId { get; set; }
        public int? DriverId { get; set; }
        public int? VehicleId { get; set; }
        public string SourceFile { get; set; }
        public bool IsActive { get; set; }
        public string StatusMessage { get; set; }
        public DateTimeOffset CreatedOn { get; set; } = DateTimeOffset.Now;
    }
}
