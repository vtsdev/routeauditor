﻿using System;
using System.Collections.Generic;
using System.Text;

namespace VTS.RouteAuditor.Common.Models
{
    public class Driver
    {
        public int Id { get; set; }
        public string DriverName { get; set; }
    }
}
