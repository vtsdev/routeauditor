﻿using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Models;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Common.Services
{
    [Injectable]
    public class FileSystemIngressService : IIngressService
    {
        private readonly ILogger _logger;
        private readonly BufferBlock<string> _pendingFiles;
        private readonly ActionBlock<string> _processFileActionBlock;

        private IIngressServiceDelegate _ingressServiceDelegate;
        private FileSystemWatcher _watcher;

        public FileSystemIngressService(ILogger<FileSystemIngressService> logger)
        {
            _logger = logger;
            _pendingFiles = new BufferBlock<string>();

            _processFileActionBlock = new ActionBlock<string>(ProcessFileAsync, new ExecutionDataflowBlockOptions { SingleProducerConstrained = true, MaxDegreeOfParallelism = 1 });
            _pendingFiles.LinkTo(_processFileActionBlock, new DataflowLinkOptions { PropagateCompletion = true });
        }


        public Task InitializeAsync(IIngressServiceOptions options, IIngressServiceDelegate ingressServiceDelegate)
        {
            if (ingressServiceDelegate == null) throw new ArgumentNullException("Ingress service delegate is required");

            if (options is FileSystemIngressOptions fileSystemOptions)
            {
                _ingressServiceDelegate = ingressServiceDelegate;

                QueueExistingFiles(fileSystemOptions.WatchedFolderPath);

                _watcher = new FileSystemWatcher(fileSystemOptions.WatchedFolderPath) { EnableRaisingEvents = true, NotifyFilter = NotifyFilters.Size };
                _watcher.Changed += OnFileAvailable;

                return Task.CompletedTask;
            }

            throw new ArgumentException($"Provided options are invalid for {GetType().Name}");
        }

        private void QueueExistingFiles(string path)
        {
            if (Directory.Exists(path))
            {
                foreach (var file in Directory.GetFiles(path))
                {
                    _pendingFiles.Post(file);
                }
            }
        }

        private void OnFileAvailable(object sender, FileSystemEventArgs e)
        {
            _pendingFiles.Post(e.FullPath);
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            cancellationToken.Register(() =>
            {
                _watcher?.Dispose();
                _pendingFiles.Complete();
            });

            return _processFileActionBlock.Completion;
        }

        private async Task ProcessFileAsync(string filePath)
        {
            var file = new FileInfo(filePath);
            var tryCount = 0;
            var maxTries = 5;

            while (tryCount < maxTries)
            {
                try
                {
                    tryCount++;

                    var data = await File.ReadAllBytesAsync(file.FullName);

                    var success = await _ingressServiceDelegate.ConsumeDataAsync(file.Name, data);
                    if (success)
                    {
                        file.Delete();

                        _logger.LogInformation($"Successfully processed {filePath}");

                        break;
                    }
                }
                catch (IOException ioex)
                {
                    await Task.Delay(1000);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Unhandled exception in {nameof(ProcessFileAsync)}");

                    break;
                }
            }
        }
    }
}
