﻿using Microsoft.Extensions.Logging;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Common.Services
{
    [Injectable]
    public class SFTPIngressService: IIngressService
    {
        private readonly ParallelOptions _parallelOptions = new () { MaxDegreeOfParallelism = Environment.ProcessorCount * 2 };
        private readonly TimeSpan _morningCheckTime = TimeSpan.FromHours(8);
        private readonly TimeSpan _eveningCheckTime = TimeSpan.FromHours(20);
        private readonly TimeSpan _downloadCacheCleanupTime = TimeSpan.FromHours(1);
        private readonly TimeSpan _batchTriggerInterval = TimeSpan.FromSeconds(15);
        private readonly TimeSpan _downloadCacheCleanupInterval = TimeSpan.FromDays(1);
        private readonly TimeSpan _downloadCacheRetentionDuration = TimeSpan.FromDays(7);

        private readonly Regex _rxRoutePlanName = new Regex(@"^(?:.*)RoutePlan_[0-9]{8}-[0-9]{6}-[0-9]{1,3}.xml$", RegexOptions.Compiled);
        private readonly Regex _rxShipmentName = new Regex(@"^(?:.*)SHIPMENT.*[0-9]{8}.csv$", RegexOptions.Compiled);

        private readonly BufferBlock<DownloadedFile> _downloadedFiles;
        private readonly TransformBlock<DownloadedFile, DownloadedFile> _processFileBlock;
        private readonly BatchBlock<DownloadedFile> _processedFilesBatchBlock;
        private readonly ActionBlock<IEnumerable<DownloadedFile>> _archiveProcessedFilesActionBlock;
        private readonly ILogger<SFTPIngressService> _logger;

        private readonly Timer _morningCheckTimer;
        private readonly Timer _eveningCheckTimer;
        private readonly Timer _batchTriggerTimer;
        private readonly Timer _downloadCacheCleanupTimer;

        private SFTPIngressOptions _config;
        private IIngressServiceDelegate _ingressServiceDelegate;

        public SFTPIngressService(ILogger<SFTPIngressService> logger)
        {
            _logger = logger;
            _downloadedFiles = new BufferBlock<DownloadedFile>();
            _processedFilesBatchBlock = new BatchBlock<DownloadedFile>(50);
            _processFileBlock = new TransformBlock<DownloadedFile, DownloadedFile>(ProcessFileAsync, new ExecutionDataflowBlockOptions { SingleProducerConstrained = true, MaxDegreeOfParallelism = 1 });
            _archiveProcessedFilesActionBlock = new ActionBlock<IEnumerable<DownloadedFile>>(ArchiveProcessedFilesActionBlockCallback, new ExecutionDataflowBlockOptions { SingleProducerConstrained = true, MaxDegreeOfParallelism = 1 });
            _downloadedFiles.LinkTo(_processFileBlock, new DataflowLinkOptions { PropagateCompletion = true });
            _processFileBlock.LinkTo(_processedFilesBatchBlock, new DataflowLinkOptions { PropagateCompletion = true });
            _processedFilesBatchBlock.LinkTo(_archiveProcessedFilesActionBlock, new DataflowLinkOptions { PropagateCompletion = true });

            _morningCheckTimer = new Timer(MorningCheckCallback);
            _eveningCheckTimer = new Timer(EveningCheckCallback);
            _batchTriggerTimer = new Timer(BatchTriggerCallback);
            _downloadCacheCleanupTimer = new Timer(DownloadCacheCleanupCallback);
        }

        #region IIngressService members

        public Task InitializeAsync(IIngressServiceOptions ingressServiceOptions, IIngressServiceDelegate ingressServiceDelegate)
        {
            if (ingressServiceDelegate == null) throw new ArgumentNullException("Ingress Service Delegate");

            _ingressServiceDelegate = ingressServiceDelegate;

            if (ingressServiceOptions is SFTPIngressOptions config)
                _config = config;

            return Task.CompletedTask;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            RestartBatchTrigger();

            var now = DateTime.Now;
            var morningCheckTime = DateTime.Today.Add(_morningCheckTime);
            if (morningCheckTime < now) morningCheckTime = morningCheckTime.AddDays(1);
            var eveningCheckTime = DateTime.Today.Add(_eveningCheckTime);
            if (eveningCheckTime < now) eveningCheckTime = eveningCheckTime.AddDays(1);
            var downloadCacheCleanupTime = DateTime.Today.Add(_downloadCacheCleanupTime);
            if (downloadCacheCleanupTime < now) downloadCacheCleanupTime = downloadCacheCleanupTime.AddDays(1);

            var morningCheckTimerStartDelay = morningCheckTime.Subtract(now);
            var eveningCheckTimerStartDelay = eveningCheckTime.Subtract(now);
            var downloadCacheCleanupTimerStartDelay = downloadCacheCleanupTime.Subtract(now);

#if DEBUG
            downloadCacheCleanupTimerStartDelay = TimeSpan.Zero;
#endif

            if (_morningCheckTimer.Change(morningCheckTimerStartDelay, TimeSpan.FromHours(24)))
                _logger.LogDebug($"Morning check timer scheduled for {morningCheckTime:MM/dd/yy HH:mm:ss} (in {morningCheckTimerStartDelay})");
            if(_eveningCheckTimer.Change(eveningCheckTimerStartDelay, TimeSpan.FromHours(24)))
                _logger.LogDebug($"Evening check timer scheduled for {eveningCheckTime:MM/dd/yy HH:mm:ss} (in {eveningCheckTimerStartDelay})");
            if (_downloadCacheCleanupTimer.Change(downloadCacheCleanupTimerStartDelay, TimeSpan.FromHours(24)))
                _logger.LogDebug($"Download cache celanup timer scheduled for {downloadCacheCleanupTime:MM/dd/yy HH:mm:ss} (in {downloadCacheCleanupTimerStartDelay})");

            return Task.CompletedTask;
        }

        #endregion



        #region SftpClient Methods

        public IEnumerable<ISftpFile> ListAllFiles(string remoteDirectory = ".")
        {
            using (var client = new SftpClient(_config.Host, _config.Port == 0 ? 22 : _config.Port, _config.UserName, _config.Password))
            {
                try
                {
                    client.Connect();
                    var retVal = client.ListDirectory(remoteDirectory);

                    _logger.LogDebug($"There are {retVal?.Count(f => f.IsRegularFile)} files pending download");

                    return retVal;
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception, $"Failed in listing files under [{remoteDirectory}]");
                    return null;
                }
                finally
                {
                    client.Disconnect();
                }
            }
        }

        public void UploadFile(string localFilePath, string remoteFilePath)
        {
            using (var client = new SftpClient(_config.Host, _config.Port == 0 ? 22 : _config.Port, _config.UserName, _config.Password))
            {
                try
                {
                    client.Connect();
                    using (var s = File.OpenRead(localFilePath))
                    {
                        client.UploadFile(s, remoteFilePath);
                    }

                    _logger.LogInformation($"Finished uploading file [{localFilePath}] to [{remoteFilePath}]");
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception, $"Failed in uploading file [{localFilePath}] to [{remoteFilePath}]");
                }
                finally
                {
                    client.Disconnect();
                }
            }
        }

        public void DownloadFile(string remoteFilePath, string localFilePath)
        {
            using (var client = new SftpClient(_config.Host, _config.Port == 0 ? 22 : _config.Port, _config.UserName, _config.Password))
            {
                try
                {
                    client.Connect();

                    var fileInfo = new FileInfo(localFilePath);
                    if (!fileInfo.Directory.Exists) fileInfo.Directory.Create();

                    using (var s = fileInfo.Create())
                    {
                        client.DownloadFile(remoteFilePath, s);
                    }

                    _logger.LogInformation($"Finished downloading file [{localFilePath}] from [{remoteFilePath}]");
                }
                catch (Exception exception)
                {
                    _logger.LogError(exception, $"Failed in downloading file [{localFilePath}] from [{remoteFilePath}]");
                }
                finally
                {
                    client.Disconnect();
                }
            }
        }
        public IEnumerable<DownloadedFile> DownloadFiles(IEnumerable<ISftpFile> files)
        {
            var retVal = new List<DownloadedFile>();

            if (files?.Count() > 0)
            {
                using (var client = new SftpClient(_config.Host, _config.Port == 0 ? 22 : _config.Port, _config.UserName, _config.Password))
                {
                    try
                    {
                        _logger.LogDebug($"Downloading {files.Count()} files");

                        client.Connect();

                        Parallel.ForEach(files, _parallelOptions, file =>
                        {
                            var remoteFilePath = file.FullName;
                            var localFilePath = Path.Combine(_config.LocalPath, file.Name);

                            try
                            {
                                var fileInfo = new FileInfo(localFilePath);
                                if (!fileInfo.Directory.Exists) fileInfo.Directory.Create();

                                if (fileInfo.Exists) fileInfo.Delete();

                                using (var s = fileInfo.Create())
                                {
                                    client.DownloadFile(remoteFilePath, s);
                                }

                                retVal.Add(new DownloadedFile { LocalPath = localFilePath, RemotePath = remoteFilePath });

                                _logger.LogInformation($"Finished downloading file [{localFilePath}] from [{remoteFilePath}]");
                            }

                            catch (Exception exception)
                            {
                                _logger.LogError(exception, $"Failed downloading file [{localFilePath}] from [{remoteFilePath}]");
                            }
                        });
                    }
                    finally
                    {
                        client.Disconnect();
                    }
                }
            }

            return retVal;
        }

        public void DeleteFile(string remoteFilePath) => DeleteFiles([remoteFilePath]);
        public void DeleteFiles(IEnumerable<string> remoteFilePaths)
        {
            if (remoteFilePaths?.Count() > 0)
            {
                using (var client = new SftpClient(_config.Host, _config.Port == 0 ? 22 : _config.Port, _config.UserName, _config.Password))
                {
                    try
                    {
                        _logger.LogDebug($"Deleting {remoteFilePaths.Count()} files from ftp");

                        client.Connect();

                        foreach (var remoteFilePath in remoteFilePaths)
                        {
                            try
                            {
                                client.DeleteFile(remoteFilePath);

                                _logger.LogInformation($"File [{remoteFilePath}] deleted.");
                            }
                            catch (Exception exception)
                            {
                                _logger.LogError(exception, $"Failed in deleting file [{remoteFilePath}]");
                            }
                        }
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, $"Failed deleting files");
                    }
                    finally
                    {
                        client.Disconnect();
                    }
                }
            }
        }

        public void ArchiveFiles(IEnumerable<string> remoteFilePaths)
        {
            if (remoteFilePaths?.Count() > 0)
            {
                using (var client = new SftpClient(_config.Host, _config.Port == 0 ? 22 : _config.Port, _config.UserName, _config.Password))
                {
                    try
                    {
                        _logger.LogDebug("Archiving {count} files", remoteFilePaths.Count());

                        client.Connect();

                        Parallel.ForEach(remoteFilePaths, _parallelOptions, remoteFilePath =>
                        {
                            try
                            {
                                var file = client.Get(remoteFilePath);
                                var archivePath = remoteFilePath.Replace(_config.RemotePath, _config.ArchivePath);
                                file.MoveTo(archivePath);

                                _logger.LogInformation("File [{remoteFilePath}] archived to [{archivePath}].", remoteFilePath, archivePath);
                            }
                            catch (Exception exception)
                            {
                                _logger.LogError(exception, "Unable to archive file [{remoteFilePath}]", remoteFilePath);
                            }
                        });
                    }
                    catch (Exception exception)
                    {
                        _logger.LogError(exception, $"Failed to archive files");
                    }
                    finally
                    {
                        client.Disconnect();
                    }
                }
            }
        }

        #endregion

        #region Private methods

        private void MorningCheckCallback(object state)
        {
            try
            {
                var files = ListAllFiles(_config.RemotePath);

                var downloadedRoutePlans = DownloadFiles(files.Where(f => _rxRoutePlanName.IsMatch(f.Name)));

                EnqueueDownloadedFilesForProcessing(downloadedRoutePlans);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unhandled exception in {nameof(MorningCheckCallback)}");
            }
        }

        private void EveningCheckCallback(object state)
        {
            try
            {
                var files = ListAllFiles(_config.RemotePath);

                var downloadedRoutePlans = DownloadFiles(files.Where(f => _rxRoutePlanName.IsMatch(f.Name)));
                EnqueueDownloadedFilesForProcessing(downloadedRoutePlans);

                var downloadedShipments = DownloadFiles(files.Where(f => _rxShipmentName.IsMatch(f.Name)));
                EnqueueDownloadedFilesForProcessing(downloadedShipments);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unhandled exception in {nameof(EveningCheckCallback)}");
            }
        }

        private void BatchTriggerCallback(object state)
        {
            try
            {
                _processedFilesBatchBlock.TriggerBatch();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unhandled exception in {nameof(BatchTriggerCallback)}");
            }

            RestartBatchTrigger();
        }

        private void DownloadCacheCleanupCallback(object state)
        {
            try
            {
                var directory = new DirectoryInfo(_config.LocalPath);
                if (!directory.Exists)
                {
                    return;
                }

                var files = directory.GetFiles();

                foreach (var fi in files)
                {
                    if (DateTime.Now.Subtract(fi.LastAccessTime) >= _downloadCacheRetentionDuration)
                        fi.Delete();
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unhandled exception in {nameof(DownloadCacheCleanupCallback)}");
            }
        }

        private void EnqueueDownloadedFilesForProcessing(IEnumerable<DownloadedFile> downloadedFiles)
        {
            foreach (var file in downloadedFiles.OrderBy(f => f.LocalPath))
                _downloadedFiles.Post(file);
        }

        private async Task<DownloadedFile> ProcessFileAsync(DownloadedFile downloadedFile)
        {
            var file = new FileInfo(downloadedFile.LocalPath);
            int tryCount = 0, maxTries = 5;

            while (tryCount < maxTries)
            {
                try
                {
                    tryCount++;

                    var data = await File.ReadAllBytesAsync(file.FullName);

                    var success = await _ingressServiceDelegate.ConsumeDataAsync(file.Name, data);
                    if (success)
                    {
                        // Instead of deleting local file, we should delete the remote file. Leave the local file to be cleaned up after 7 days.
                        //file.Delete();

                        _logger.LogInformation($"Successfully processed {downloadedFile.LocalPath}");
                    }
                    else
                    {
                        _logger.LogInformation($"Nothing was processed from {downloadedFile.LocalPath}");
                    }

                    return downloadedFile;
                }
                catch (IOException)
                {
                    await Task.Delay(1000);
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Unhandled exception in {nameof(ProcessFileAsync)}");

                    break;
                }
            }

            return null;
        }

        private void RestartBatchTrigger()
        {
            try
            {
                _batchTriggerTimer.Change(_batchTriggerInterval, Timeout.InfiniteTimeSpan);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unhandled exception in {nameof(RestartBatchTrigger)}");
            }
        }

        private void ArchiveProcessedFilesActionBlockCallback(IEnumerable<DownloadedFile> collection)
        {
            RestartBatchTrigger();

            try
            {
                //DeleteFiles(collection.Select(f => f.RemotePath));
                ArchiveFiles(collection.Select(f => f.RemotePath));
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unhandled exception in {nameof(ArchiveProcessedFilesActionBlockCallback)}");
            }
        }

        #endregion

        public class DownloadedFile
        {
            public string LocalPath { get; set; }
            public string RemotePath { get; set; }
        }
    }
}
