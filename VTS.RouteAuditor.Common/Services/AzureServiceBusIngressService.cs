﻿using Azure.Messaging.ServiceBus;
using Microsoft.Extensions.Logging;
using System;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Common.Services
{
    [Injectable]
    public class AzureServiceBusIngressService : IIngressService
    {
        private readonly ILogger<AzureServiceBusIngressService> _logger;
        private ServiceBusClient _client;
        private ServiceBusProcessor _processor;
        private IIngressServiceDelegate _ingressServiceDelegate;

        public AzureServiceBusIngressService(ILogger<AzureServiceBusIngressService> logger)
        {
            _logger = logger;
        }
        public Task InitializeAsync(IIngressServiceOptions options, IIngressServiceDelegate ingressServiceDelegate)
        {
            if (ingressServiceDelegate == null) throw new ArgumentNullException("Ingress service delegate is required");

            if (options is AzureServiceBusOptions azureServiceBusOptions)
            {
                _ingressServiceDelegate = ingressServiceDelegate;

                var messageHandlerOptions = new ServiceBusProcessorOptions
                {
                    MaxConcurrentCalls = 1,
                    AutoCompleteMessages = false
                };

                _client = new ServiceBusClient(azureServiceBusOptions.ConnectionString);
                _processor = _client.CreateProcessor(azureServiceBusOptions.QueueName, messageHandlerOptions);
                _processor.ProcessMessageAsync += ProcessMessagesAsync;
                _processor.ProcessErrorAsync += ExceptionReceivedHandler;

                return Task.CompletedTask;
            }

            throw new ArgumentException($"Provided options are invalid for {GetType().Name}");
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            var tcs = new TaskCompletionSource<object>(TaskCreationOptions.RunContinuationsAsynchronously);

            cancellationToken.Register(async () =>
            {
                await _processor.CloseAsync();
                await _processor.DisposeAsync();
                await _client.DisposeAsync();
                _logger.LogInformation("Message queue closed");

                tcs.TrySetCanceled();
            });

            return tcs.Task;
        }

        private async Task ProcessMessagesAsync(ProcessMessageEventArgs args)
        {
            try
            {
                var message = args.Message;

                _logger.LogDebug($"Received message: SequenceNumber:{message.SequenceNumber} Body:{Encoding.UTF8.GetString(message.Body)}");

                var success = await _ingressServiceDelegate.ConsumeDataAsync(message.MessageId, message.Body.ToArray());
                if (success)
                {
                    await args.CompleteMessageAsync(message);
                    //await _queueClient.CompleteAsync(message.SystemProperties.LockToken);

                    _logger.LogInformation($"Successfully processed message: SequenceNumber:{message.SequenceNumber}");
                }
                else
                {
                    await args.AbandonMessageAsync(message);
                    //await _queueClient.AbandonAsync(message.SystemProperties.LockToken);
                }
            }
            catch (TaskCanceledException)
            {
                _logger.LogInformation("ProcessMessagesAsync task cancelled");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unhandled exception in {nameof(ProcessMessagesAsync)}");
            }
        }

        private Task ExceptionReceivedHandler(ProcessErrorEventArgs args)
        {
            var sb = new StringBuilder();

            sb.AppendLine($"Message handler encountered an exception {args.Exception}.");
            sb.AppendLine("Exception context for troubleshooting:");
            sb.AppendLine($"- Error Source: {args.ErrorSource}");
            sb.AppendLine($"- Entity Path: {args.EntityPath}");
            sb.AppendLine($"- Fully Qualified Namespace: {args.FullyQualifiedNamespace}");

            _logger.LogError(sb.ToString());

            return Task.CompletedTask;
        }
    }
}
