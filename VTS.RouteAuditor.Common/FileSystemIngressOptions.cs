﻿using System;
using System.Collections.Generic;
using System.Text;
using VTS.RouteAuditor.Common.Interfaces;

namespace VTS.RouteAuditor.Common
{
    public class FileSystemIngressOptions : IIngressServiceOptions
    {
        public string WatchedFolderPath { get; }

        public FileSystemIngressOptions(string watchedFolderPath)
        {
            WatchedFolderPath = watchedFolderPath;
        }
    }
}
