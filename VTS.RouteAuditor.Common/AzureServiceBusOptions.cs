﻿using System;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Common
{
    public class AzureServiceBusOptions : IIngressServiceOptions
    {
        public string ConnectionString { get; }
        public string QueueName { get; }

        public AzureServiceBusOptions(string connectionString, string queueName)
        {
            ConnectionString = connectionString;
            QueueName = queueName;
        }
    }
}
