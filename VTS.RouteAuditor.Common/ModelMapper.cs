﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace VTS.RouteAuditor.Common
{
    public sealed class ModelMapper
    {
        private volatile static ModelMapper _instance;
        private static object _syncRoot = new object();

        private readonly IMapper _mapper;

        private ModelMapper()
        {
            _mapper = Initialize();
        }

        public static ModelMapper Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new ModelMapper();
                    }
                }

                return _instance;
            }
        }

        public T Map<T>(object source)
        {
            return _mapper.Map<T>(source);
        }

        public IEnumerable<T> MapCollection<T>(IEnumerable<object> source)
        {
            return source?.Select(s => Map<T>(s)).ToList();
        }

        private IMapper Initialize()
        {
            var profileTypes = AppDomain.CurrentDomain
                .GetAssemblies()
                .Where(a => a.FullName.StartsWith("VTS.RouteAuditor"))
                .SelectMany(a => a.GetTypes())
                .Where(t => typeof(Profile).IsAssignableFrom(t) && !t.IsAbstract);

            var assemblies = profileTypes.Select(t => t.Assembly).Distinct();

            var config = new MapperConfiguration(cfg =>
            {
                foreach (var profileType in profileTypes)
                {
                    var profile = (Profile)Activator.CreateInstance(profileType);

                    cfg.AddProfile(profile);
                }
            });
            return config.CreateMapper();
        }
    }
}
