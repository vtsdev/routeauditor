﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using VTS.RouteAuditor.Common.Interfaces;

namespace VTS.RouteAuditor.Common
{
    public class SFTPIngressOptions : IIngressServiceOptions
    {
        public string Host { get; }
        public int Port { get; }
        public string UserName { get; }
        public string Password { get; }
        public string RemotePath { get; }
        public string ArchivePath { get; }
        public string LocalPath { get; }

        public SFTPIngressOptions(string host, string username, string password, int port = 22, string remotePath = null, string localPath = null)
        {
            Host = host;
            UserName = username;
            Password = password;
            Port = port;
            RemotePath = remotePath ?? ".";
            ArchivePath = $"{RemotePath}/archive";
            LocalPath = localPath ?? Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "Downloads");
        }
    }
}
