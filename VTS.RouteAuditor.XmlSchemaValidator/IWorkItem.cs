﻿using System.Threading.Tasks;

namespace VTS.RouteAuditor.XmlSchemaValidator
{
    public interface IWorkItem
    {
        Task RunAsync();
    }
}