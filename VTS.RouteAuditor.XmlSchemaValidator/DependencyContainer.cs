﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.XmlSchemaValidator
{
    internal static class DependencyContainer
    {
        public static IServiceCollection Configure(IServiceCollection services, IConfiguration config)
        {
            services.LoadInjectableServices(type => type.Namespace?.StartsWith("VTS.RouteAuditor") ?? false);
            services.AddTransient<IWorkItem, WorkItem>();

            return services;
        }
    }
}
