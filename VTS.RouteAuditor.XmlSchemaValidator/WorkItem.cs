﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Models;

namespace VTS.RouteAuditor.XmlSchemaValidator
{
    public class WorkItem : IWorkItem
    {
        private readonly ILogger<WorkItem> _log;
        private readonly IConfiguration _config;
        private readonly IParseData<RoutePlan> _routeParser;

        public WorkItem(ILogger<WorkItem> log, IConfiguration config, IParseData<RoutePlan> routeParser)
        {
            _log = log;
            _config = config;
            _routeParser = routeParser;
        }
        public async Task RunAsync()
        {
            var path = @"E:\Temp\CONA Routes";
            var files = Directory.EnumerateFiles(path);

            //foreach (var file in files.Where(f => Regex.IsMatch(f, @"^[^_]+_[0-9]{8}-[0-9]{6}-[0-9]{3}.*")))
            foreach(var file in files.Where(f => Regex.IsMatch(f, @"^.*GEOTABRoutePlan_.*\.xml")))
            {
                _log.LogInformation($"Validating {file}");

                var data = await File.ReadAllBytesAsync(file);
                try
                {
                    RoutePlan routePlan = await _routeParser.ParseAsync(Path.GetFileName(file), data);
                    _log.LogInformation($"Successfully parsed {file}");
                }
                catch (Exception ex)
                {
                    var msg = ex.Message;
                    if (ex.InnerException != null)
                    {
                        msg = $"{msg}; {ex.InnerException.Message}";
                    }
                    _log.LogError($"Unhandled exception while parsing {file}: {msg}");
                }
            }
        }
    }
}
