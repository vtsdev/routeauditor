﻿using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Interfaces.DataProviders;
using VTS.RouteAuditor.Common.Models;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Core
{
    [Injectable]
    public class Engine : BackgroundService
    {
        private readonly IServiceScopeFactory _serviceScopeFactory;
        private readonly IAccountProvider _accountsProvider;
        private readonly ILogger<Engine> _logger;

        public Engine(IServiceScopeFactory serviceScopeFactory, IAccountProvider accountsProvider, ILogger<Engine> logger)
        {
            _serviceScopeFactory = serviceScopeFactory;
            _accountsProvider = accountsProvider;
            _logger = logger;
        }

        protected override async Task ExecuteAsync(CancellationToken stoppingToken)
        {
            var accounts = await _accountsProvider.GetAllAsync();

            using (var scope = _serviceScopeFactory.CreateScope())
            {
                await RunWorkersAsync(scope, accounts, stoppingToken).ConfigureAwait(false);
            }

            _logger.LogInformation("All worker tasks completed");
        }

        private Task RunWorkersAsync(IServiceScope scope, IEnumerable<Account> accounts, CancellationToken stoppingToken)
        {
            var tasks = new List<Task>();

            foreach (var account in accounts)
            {
                try
                {
                    var workerType = Type.GetType(account.WorkerType);
                    if (workerType != null)
                    {
                        var worker = scope.ServiceProvider.GetRequiredService(workerType) as IRouteAuditorWorker;

                        if (worker != null)
                            tasks.Add(worker.RunAsyncAsync(account, stoppingToken));
                        else
                            throw new TypeLoadException($"Worker type \"{account.WorkerType}\" does not implement interface {nameof(IRouteAuditorWorker)}");
                    }
                    else
                        throw new TypeLoadException($"Worker type \"{account.WorkerType}\" not found");
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Unhandled exception in {nameof(ExecuteAsync)} for Account: {account.Id}");
                }
            }

            return Task.WhenAll(tasks);
        }
    }
}
