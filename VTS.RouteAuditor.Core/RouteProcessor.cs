﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Models;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Core
{
    [Injectable]
    internal class RouteProcessor : IProcessRoutes
    {
        private readonly ILogger<RouteProcessor> _logger;

        public RouteProcessor(ILogger<RouteProcessor> logger)
        {
            _logger = logger;
        }

        public async Task<bool> ProcessAsync(RoutePlan route)
        {
            try
            {
                var plannedStops = await GetPlannedStopsAsync(route);
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unhandled exception in {nameof(ProcessAsync)}");
            }

            return false;
        }

        private Task<IEnumerable<PlannedStop>> GetPlannedStopsAsync(RoutePlan route)
        {
            return default;
        }
    }
}
