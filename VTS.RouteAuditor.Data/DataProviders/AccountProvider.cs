﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Interfaces.DataProviders;
using VTS.RouteAuditor.Common.Models;
using VTS.RouteAuditor.Data.DataProviders;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Data.Repositories
{
    [Injectable]
    internal class AccountProvider : DataProviderBase, IAccountProvider
    {
        public AccountProvider(IConnectionStringProvider connectionStringProvider, ILogger<AccountProvider> logger) : base(connectionStringProvider, logger)
        {
        }

        public async Task<IEnumerable<Account>> GetAllAsync()
        {
            IEnumerable<Account> retVal = null;
            var strCommand = "dbo.Account_GetList";

            using (var conn = GetConnection())
            using (var cmd = GetProcedureCommand(conn, strCommand))
            {
                try
                {
                    await conn.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        retVal = ModelMapper.Instance.MapReader<Account>(reader);
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Unhandled exception in {nameof(GetAllAsync)}");
                }
            }

            return retVal;
        }
    }
}
