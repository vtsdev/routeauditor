﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Interfaces.DataProviders;
using VTS.RouteAuditor.Common.Models;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Data.DataProviders
{
    [Injectable]
    internal class LandmarkProvider : DataProviderBase, ILandmarkProvider
    {
        public LandmarkProvider(IConnectionStringProvider connectionStringProvider, ILogger<LandmarkProvider> logger) : base(connectionStringProvider, logger)
        {
        }
        public async Task<Landmark> GetLandmarkByCustomerNumberAsync(int accountId, string customerNumber)
        {
            Landmark retVal = null;
            var parameters = new Dictionary<string, object>
            {
                { "AccountId", accountId },
                { "CustomerNumber", customerNumber }
            };

            using (var conn = GetConnection())
            using (var cmd = GetProcedureCommand(conn, "dbo.Landmark_Get_ByCustomerNumber", parameters))
            {
                try
                {
                    await conn.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            retVal = ModelMapper.Instance.MapRecord<Landmark>(reader);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Unhandled exception in {nameof(GetLandmarkByCustomerNumberAsync)}");
                }
            }

            return retVal;
        }
    }
}
