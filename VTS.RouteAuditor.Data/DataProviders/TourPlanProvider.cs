﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Interfaces.DataProviders;
using VTS.RouteAuditor.Common.Models;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Data.DataProviders
{
    [Injectable]
    internal class TourPlanProvider : DataProviderBase, ITourPlanProvider
    {
        private readonly IPlannedStopProvider _plannedStopProvider;

        public TourPlanProvider(IPlannedStopProvider plannedStopProvider, IConnectionStringProvider connectionStringProvider, ILogger<TourPlanProvider> logger) : base(connectionStringProvider, logger)
        {
            _plannedStopProvider = plannedStopProvider;
        }

        public async Task<TourPlan> GetByIdentifierAsync(int accountId, string tourIdentifier, int? tourSequence = null)
        {
            TourPlan retVal = null;
            var parameters = new Dictionary<string, object>
            {
                { "AccountId", accountId },
                { "TourIdentifier", tourIdentifier }
            };

            if (tourSequence.HasValue) parameters.Add("TourSequence", tourSequence);

            using (var conn = GetConnection())
            using (var cmd = GetProcedureCommand(conn, "dbo.TourPlan_Get_ByTourIdentifier", parameters))
            {
                await conn.OpenAsync();

                using (var reader = await cmd.ExecuteReaderAsync(System.Data.CommandBehavior.CloseConnection))
                {
                    if (await reader.ReadAsync())
                    {
                        retVal = ModelMapper.Instance.MapRecord<TourPlan>(reader);
                    }
                }
            }

            return retVal;
        }

        public async Task<IEnumerable<TourPlan>> GetListAsync(int accountId, long? routePlanId)
        {
            IEnumerable<TourPlan> retVal = new List<TourPlan>();
            var parameters = new Dictionary<string, object>
            {
                { "AccountId", accountId }
            };

            if (routePlanId.HasValue) parameters.Add("RoutePlanId", routePlanId.Value);

            using (var conn = GetConnection())
            using (var cmd = GetProcedureCommand(conn, "dbo.TourPlan_GetList", parameters))
            {
                await conn.OpenAsync();

                using (var reader = await cmd.ExecuteReaderAsync())
                {
                    retVal = ModelMapper.Instance.MapReader<TourPlan>(reader);
                }
            }

            return retVal;
        }

        public async Task<TourPlan> InsertAsync(TourPlan item)
        {
            var parameters = new Dictionary<string, object>
            {
                { "RoutePlanId", item.RoutePlanId },
                { "TourIdentifier", item.TourIdentifier },
                { "TourSequence", item.TourSequence },
                { "ShipmentNumber", item.ShipmentNumber },
                { "StartTime", item.StartTime },
                { "EndTime", item.EndTime },
                { "DriveTimeMinutes", item.DriveTime.TotalMinutes },
                { "WaitTimeMinutes", item.WaitTime.TotalMinutes },
                { "WorkTimeMinutes", item.WorkTime.TotalMinutes },
                { "Distance", item.Distance },
                { "SourceFile", item.SourceFile }
            };

            using (var conn = GetConnection())
            using (var cmd = GetProcedureCommand(conn, "dbo.TourPlan_Insert", parameters))
            {
                var outParam = AddOutputParameter(cmd, "Id", System.Data.DbType.Int64);

                await conn.OpenAsync();

                await cmd.ExecuteNonQueryAsync();

                var tourPlanId = (long)outParam.Value;
                if (tourPlanId > 0)
                {
                    item.Id = tourPlanId;
                    foreach (var plannedStop in item.PlannedStops)
                    {
                        plannedStop.TourPlanId = item.Id;

                        await _plannedStopProvider.InsertAsync(plannedStop);
                    }
                }
                else
                {
                    _logger.LogInformation($"Tour Plan [ RoutePlanId: {item.RoutePlanId}; TourIdentifier: {item.TourIdentifier}; TourSequence: {item.TourSequence} ] was not inserted.");
                }
            }

            return item;
        }
    }
}
