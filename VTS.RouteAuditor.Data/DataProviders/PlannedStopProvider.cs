﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Interfaces.DataProviders;
using VTS.RouteAuditor.Common.Models;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Data.DataProviders
{
    [Injectable]
    internal class PlannedStopProvider : DataProviderBase, IPlannedStopProvider
    {
        public PlannedStopProvider(IConnectionStringProvider connectionStringProvider, ILogger<PlannedStopProvider> logger) : base(connectionStringProvider, logger)
        {
        }

        public async Task<PlannedStop> InsertAsync(PlannedStop item)
        {
            var parameters = new Dictionary<string, object>
            {
                { "TourPlanId", item.TourPlanId },
                { "TaskId", item.TaskId },
                { "Name", item.Name },
                { "StopNumber", item.StopNumber }
            };

            if (!string.IsNullOrEmpty(item.CustomerNumber)) parameters.Add("CustomerNumber", item.CustomerNumber);
            if (item.LandmarkId.HasValue) parameters.Add("LandmarkId", item.LandmarkId.Value);
            if (item.ArrivalTime.HasValue) parameters.Add("ArrivalTime", item.ArrivalTime.Value);
            if (item.DepartureTime.HasValue) parameters.Add("DepartureTime", item.DepartureTime.Value);
            if (item.Cases.HasValue) parameters.Add("Cases", item.Cases.Value);
            if (item.Cubes.HasValue) parameters.Add("Cubes", item.Cubes.Value);
            if (item.WorkTime.HasValue) parameters.Add("WorkTimeMinutes", item.WorkTime.Value.TotalMinutes);

            using (var conn = GetConnection())
            using (var cmd = GetProcedureCommand(conn, "dbo.PlannedStop_Insert", parameters))
            {
                var outParam = AddOutputParameter(cmd, "Id", System.Data.DbType.Int64);

                await conn.OpenAsync();

                if (await cmd.ExecuteNonQueryAsync() > 0)
                {
                    item.Id = (long)outParam.Value;
                }
            }

            return item;
        }
    }
}
