﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Interfaces.DataProviders;
using VTS.RouteAuditor.Common.Models;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Data.DataProviders
{
    [Injectable]
    internal class RouteAssignmentProvider : DataProviderBase, IRouteAssignmentProvider
    {
        public RouteAssignmentProvider(IConnectionStringProvider connectionStringProvider, ILogger<RouteAssignmentProvider> logger) : base(connectionStringProvider, logger)
        {
        }

        public async Task<RouteAssignment> InsertAsync(RouteAssignment item)
        {
            var parameters = new Dictionary<string, object>
            {
                { "AccountId", item.AccountId },
                { "SourceFile", item.SourceFile },
                { "IsActive", item.IsActive },
                { "CreatedOn", item.CreatedOn }
            };

            if (item.RoutePlanId.HasValue) parameters.Add("RoutePlanId", item.RoutePlanId.Value);
            if (item.TourPlanId.HasValue) parameters.Add("TourPlanId", item.TourPlanId.Value);
            if (item.DriverId.HasValue) parameters.Add("DriverId", item.DriverId.Value);
            if (item.VehicleId.HasValue) parameters.Add("VehicleId", item.VehicleId.Value);
            if (!string.IsNullOrEmpty(item.StatusMessage)) parameters.Add("StatusMessage", item.StatusMessage);

            using (var conn = GetConnection())
            using (var cmd = GetProcedureCommand(conn, "dbo.RouteAssignment_Insert", parameters))
            {
                var outParam = AddOutputParameter(cmd, "Id", System.Data.DbType.Int64);

                await conn.OpenAsync();

                if (await cmd.ExecuteNonQueryAsync() > 0)
                {
                    item.Id = (long)outParam.Value;
                }
            }

            return item;
        }
    }
}
