﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Interfaces.DataProviders;
using VTS.RouteAuditor.Common.Models;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Data.DataProviders
{
    [Injectable]
    internal class RoutePlanProvider : DataProviderBase, IRoutePlanProvider
    {
        private readonly ITourPlanProvider _tourPlanProvider;

        public RoutePlanProvider(ITourPlanProvider tourPlanProvider, IConnectionStringProvider connectionStringProvider, ILogger<RoutePlanProvider> logger) : base(connectionStringProvider, logger)
        {
            _tourPlanProvider = tourPlanProvider;
        }

        public Task<RoutePlan> GetAsync(long id)
        {
            throw new NotImplementedException();
        }

        public async Task<RoutePlan> GetByIdentifierAsync(int accountId, string routeIdentifier, DateTime routeDate)
        {
            RoutePlan retVal = null;
            var parameters = new Dictionary<string, object>
            {
                { "AccountId", accountId },
                { "RouteIdentifier", routeIdentifier },
                { "RouteDate", routeDate }
            };

            using (var conn = GetConnection())
            using (var cmd = GetProcedureCommand(conn, "dbo.RoutePlan_Get_ByRouteIdentifier", parameters))
            {
                await conn.OpenAsync();

                using (var reader = await cmd.ExecuteReaderAsync(System.Data.CommandBehavior.CloseConnection))
                {
                    if (await reader.ReadAsync())
                    {
                        retVal = ModelMapper.Instance.MapRecord<RoutePlan>(reader);
                    }
                }
            }

            return retVal;
        }

        public async Task<RoutePlan> InsertAsync(RoutePlan item)
        {
            var parameters = new Dictionary<string, object>
            {
                { "AccountId", item.AccountId },
                { "RouteIdentifier", item.RouteIdentifier },
                { "RouteDate", item.RouteDate },
                { "SourceFile", item.SourceFile }
            };

            using (var conn = GetConnection())
            using (var cmd = GetProcedureCommand(conn, "dbo.RoutePlan_Insert", parameters))
            {
                var outParam = AddOutputParameter(cmd, "Id", System.Data.DbType.Int64);

                await conn.OpenAsync();

                await cmd.ExecuteNonQueryAsync();

                var routePlanId = (long)outParam.Value;
                if (routePlanId > 0)
                {
                    item.Id = routePlanId;
                    foreach (var tourPlan in item.Tours)
                    {
                        tourPlan.RoutePlanId = item.Id;

                        await _tourPlanProvider.InsertAsync(tourPlan);
                    }
                }
                else
                {
                    _logger.LogInformation($"Route Plan [ AccountId: {item.AccountId}; RouteIdentifier: {item.RouteIdentifier}; RouteDate: {item.RouteDate:MM-dd-yyyy} ] was not inserted.");
                }
            }

            return item;
        }

        public Task<RoutePlan> UpdateAsync(RoutePlan item)
        {
            throw new NotImplementedException();
        }
    }
}
