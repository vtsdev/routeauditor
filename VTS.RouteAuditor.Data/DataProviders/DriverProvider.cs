﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Interfaces.DataProviders;
using VTS.RouteAuditor.Common.Models;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.Data.DataProviders
{
    [Injectable]
    internal class DriverProvider : DataProviderBase, IDriverProvider
    {
        public DriverProvider(IConnectionStringProvider connectionStringProvider, ILogger<DriverProvider> logger) : base(connectionStringProvider, logger)
        {
        }
        public async Task<Driver> GetByDriverIdentifier(int accountId, string driverIdentifier)
        {
            Driver retVal = null;
            var parameters = new Dictionary<string, object>
            {
                { "AccountId", accountId },
                { "DriverIdentifier", driverIdentifier }
            };

            using (var conn = GetConnection())
            using (var cmd = GetProcedureCommand(conn, "dbo.Driver_Get_ByIdentifier", parameters))
            {
                try
                {
                    await conn.OpenAsync();

                    using (var reader = await cmd.ExecuteReaderAsync())
                    {
                        if (reader.Read())
                        {
                            retVal = ModelMapper.Instance.MapRecord<Driver>(reader);
                        }
                    }
                }
                catch (Exception ex)
                {
                    _logger.LogError(ex, $"Unhandled exception in {nameof(GetByDriverIdentifier)}");
                }
            }

            return retVal;
        }
    }
}
