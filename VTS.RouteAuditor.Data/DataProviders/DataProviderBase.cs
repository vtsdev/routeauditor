﻿using Microsoft.Data.SqlClient;
using Microsoft.Extensions.Logging;
using System.Collections.Generic;
using System.Data.Common;
using System.Linq;
using VTS.RouteAuditor.Common.Interfaces;

namespace VTS.RouteAuditor.Data.DataProviders
{
    internal abstract class DataProviderBase
    {
        private readonly string _connStr;
        protected readonly ILogger _logger;

        protected virtual string ConnectionStringName => "RouteAuditor";

        protected DataProviderBase(IConnectionStringProvider connectionStringProvider, ILogger logger)
        {
            _connStr = connectionStringProvider.GetConnectionString(ConnectionStringName);
            _logger = logger;
        }

        protected DbConnection GetConnection()
        {
            return new SqlConnection(_connStr);
        }

        protected DbCommand GetProcedureCommand(DbConnection conn, string storedProcedureName, Dictionary<string, object> parameters = null)
        {
            return GetCommand(conn, storedProcedureName, System.Data.CommandType.StoredProcedure, parameters);
        }

        protected DbParameter AddOutputParameter(DbCommand cmd, string paramName, System.Data.DbType paramType)
        {
            var outParam = cmd.CreateParameter();
            outParam.Direction = System.Data.ParameterDirection.Output;
            outParam.ParameterName = paramName;
            outParam.DbType = paramType;

            cmd.Parameters.Add(outParam);

            return outParam;
        }

        private DbCommand GetCommand(DbConnection conn, string commandText, System.Data.CommandType commandType, IEnumerable<KeyValuePair<string, object>> parameters = null)
        {
            var dbCommand = conn.CreateCommand();
            dbCommand.CommandText = commandText;
            dbCommand.CommandType = commandType;

            if (parameters != null)
            {
                dbCommand.Parameters.AddRange(parameters.Select(p => new SqlParameter(p.Key, p.Value)).ToArray());
            }

            return dbCommand;
        }
    }
}
