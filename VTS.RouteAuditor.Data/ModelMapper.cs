﻿using AutoMapper;
using AutoMapper.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using VTS.RouteAuditor.Data.AutomapperProfiles;

namespace VTS.RouteAuditor.Data
{
    internal sealed class ModelMapper
    {
        private volatile static ModelMapper _instance;
        private static object _syncRoot = new object();

        private readonly IMapper _mapper;

        private ModelMapper()
        {
            _mapper = Initialize();
        }

        public static ModelMapper Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (_syncRoot)
                    {
                        if (_instance == null)
                            _instance = new ModelMapper();
                    }
                }

                return _instance;
            }
        }

        public IEnumerable<T> MapReader<T>(IDataReader reader)
        {
            return _mapper.Map<IDataReader, IEnumerable<T>>(reader);
        }

        public T MapRecord<T>(IDataRecord record)
        {
            return _mapper.Map<IDataRecord, T>(record);
        }

        private IMapper Initialize()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.AddDataReaderMapping();
                cfg.AddProfile(typeof(MainProfile));
            });
            return config.CreateMapper();
        }
    }
}
