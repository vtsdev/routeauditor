﻿using AutoMapper;
using System;
using System.Data;
using VTS.RouteAuditor.Common.Models;

namespace VTS.RouteAuditor.Data.AutomapperProfiles
{
    internal class MainProfile : Profile
    {
        public MainProfile()
        {
            CreateMap<IDataRecord, Account>();
            CreateMap<IDataRecord, Driver>();
            CreateMap<IDataRecord, Landmark>();
            CreateMap<IDataRecord, PlannedStop>();
            CreateMap<IDataRecord, RoutePlan>();
            CreateMap<IDataRecord, TourPlan>()
                .ForMember(dst => dst.DriveTime, opt => opt.MapFrom(src => TimeSpan.FromMinutes(src.GetInt32(src.GetOrdinal("DriveTimeMinutes")))))
                .ForMember(dst => dst.WaitTime, opt => opt.MapFrom(src => TimeSpan.FromMinutes(src.GetInt32(src.GetOrdinal("WaitTimeMinutes")))))
                .ForMember(dst => dst.WorkTime, opt => opt.MapFrom(src => TimeSpan.FromMinutes(src.GetInt32(src.GetOrdinal("WorkTimeMinutes")))));
        }
    }
}
