﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.Common.Interfaces.DataProviders;
using VTS.RouteAuditor.Common.Models;
using VTS.RouteAuditor.Common.Services;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.CONA
{
    [Injectable]
    internal class RouteAuditorWorker : IRouteAuditorWorker, IIngressServiceDelegate
    {
        private readonly IIngressService _ingressService;
        private readonly RouteParser _routeParser;
        private readonly ShipmentParser _shipmentParser;
        private readonly ILandmarkProvider _landmarksProvider;
        private readonly IRoutePlanProvider _routePlanProvider;
        private readonly ITourPlanProvider _tourPlanProvider;
        private readonly IDriverProvider _driverProvider;
        private readonly IRouteAssignmentProvider _routeAssignmentProvider;
        private readonly ILogger<RouteAuditorWorker> _logger;
        private bool _isInitialized;
        private Account _account;
        private readonly string _executablePath;

        public RouteAuditorWorker(
            IEnumerable<IIngressService> ingressServices,
            RouteParser routeParser, 
            ShipmentParser shipmentParser, 
            ILandmarkProvider landmarksProvider, 
            IRoutePlanProvider routePlanProvider, 
            ITourPlanProvider tourPlanProvider,
            IDriverProvider driverProvider, 
            IRouteAssignmentProvider routeAssignmentProvider,
            ILogger<RouteAuditorWorker> logger)
        {
            _ingressService = ingressServices.FirstOrDefault(s => s.GetType() == typeof(SFTPIngressService));

#if DEBUG
            _ingressService = ingressServices.FirstOrDefault(s => s.GetType() == typeof(FileSystemIngressService));
#endif

            _routeParser = routeParser;
            _shipmentParser = shipmentParser;
            _landmarksProvider = landmarksProvider;
            _routePlanProvider = routePlanProvider;
            _tourPlanProvider = tourPlanProvider;
            _driverProvider = driverProvider;
            _routeAssignmentProvider = routeAssignmentProvider;
            _logger = logger;
            _isInitialized = false;

            _executablePath = System.IO.Directory.GetCurrentDirectory();
        }

        #region IRouteAuditorWorker members
        public async Task RunAsyncAsync(Account account, CancellationToken cancellationToken)
        {
            try
            {
                cancellationToken.ThrowIfCancellationRequested();

                _account = account;
                IIngressServiceOptions options = new SFTPIngressOptions(_account.SFTPHost, _account.SFTPUsername, _account.SFTPPassword, remotePath: _account.SFTPRootPath, localPath: $@"{_executablePath}\CONADownloads\{_account.MobileGroupId}");

#if DEBUG
                options = new FileSystemIngressOptions(@"E:\Temp\CONA Routes");
#endif

                await _ingressService.InitializeAsync(options, this);

                _logger.LogInformation($"Initialized {nameof(RouteAuditorWorker)} for Queue {_account.SFTPHost}: {_account.SFTPUsername}");

                _isInitialized = true;

                await _ingressService.StartAsync(cancellationToken);
            }
            catch (TaskCanceledException)
            {
                _logger.LogInformation($"{nameof(RouteAuditorWorker)} has been cancelled");
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unhandled exception in {nameof(RunAsyncAsync)}");
            }
        }
        #endregion

        #region IIngressServiceDelegate members
        public async Task<bool> ConsumeDataAsync(string dataIdentifier, byte[] data)
        {
            bool success = false;

            if (await _routeParser.CanParseAsync(dataIdentifier, data))
            {
                var route = await _routeParser.ParseAsync(dataIdentifier, data);
                if (route != null)
                {
                    route.AccountId = _account.Id;
                    await EnrichPlannedStopsAsync(route);

                    success = await SaveRouteAsync(route);
                }
            }
            else if (await _shipmentParser.CanParseAsync(dataIdentifier, data))
            {
                var shipments = await _shipmentParser.ParseAsync(dataIdentifier, data);
                success = await AssignRoutePlansAsync(dataIdentifier, shipments);
            }

            return success;
        }
        #endregion

        private async Task EnrichPlannedStopsAsync(RoutePlan routePlan)
        {
            try
            {
                foreach (var tour in routePlan.Tours)
                {
                    tour.SourceFile = routePlan.SourceFile;

                    foreach (var stop in tour.PlannedStops.Where(s => !string.IsNullOrEmpty(s.CustomerNumber)))
                    {
                        var accountNo = stop.CustomerNumber.TrimStart('0');
                        var landmark = await _landmarksProvider.GetLandmarkByCustomerNumberAsync(_account.Id, accountNo);

                        if (landmark != null)
                        {
                            stop.LandmarkId = landmark.Id;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unhandled exception in {nameof(EnrichPlannedStopsAsync)}");
            }
        }
        private async Task<bool> SaveRouteAsync(RoutePlan routePlan)
        {
            var success = false;

            try
            {
                if (routePlan.Id > 0)
                    routePlan = await _routePlanProvider.UpdateAsync(routePlan);
                else
                    routePlan = await _routePlanProvider.InsertAsync(routePlan);

                success = routePlan != null;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unhandled exception in {nameof(SaveRouteAsync)}");
            }

            return success;
        }
        private async Task<bool> AssignRoutePlansAsync(string dataIdentifier, IEnumerable<IShipment> shipments)
        {
            var success = false;

            if (shipments != null)
            {
                foreach (var shipment in shipments)
                {
                    try
                    {
                        var routeAssignment = new RouteAssignment { SourceFile = dataIdentifier, AccountId = _account.Id };

                        var routePlan = await _routePlanProvider.GetByIdentifierAsync(_account.Id, shipment.RouteIdentifier, shipment.RouteDate);
                        if (routePlan != null)
                        {
                            routeAssignment.RoutePlanId = routePlan.Id;

                            var tourPlans = await _tourPlanProvider.GetListAsync(_account.Id, routePlan.Id);
                            var tourPlan = tourPlans.FirstOrDefault(t => t.ShipmentNumber == shipment.ShipmentIdentifier);

                            routeAssignment.TourPlanId = tourPlan?.Id;

                            if (tourPlan?.IsActive == true)
                            {
                                var driverIdentifier = shipment.DriverIdentifier;
                                var driver = await _driverProvider.GetByDriverIdentifier(_account.Id, driverIdentifier);
                                if (driver != null)
                                {
                                    routeAssignment.DriverId = driver.Id;
                                    routeAssignment.IsActive = true;
                                }
                                else
                                {
                                    routeAssignment.StatusMessage = $"Driver with an identifier {driverIdentifier} was not found";
                                }
                            }
                            else
                            {
                                if (tourPlan == null)
                                    routeAssignment.StatusMessage = $"Ignoring assignments for a non-existent tour - TourIdentifier: {shipment.TourIdentifier}";
                                else
                                    routeAssignment.StatusMessage = $"Ignoring assignments for an inactive tour - TourIdentifier: {shipment.TourIdentifier}";
                            }
                        }
                        else
                        {
                            routeAssignment.StatusMessage = $"Route with an identifier {shipment.RouteIdentifier} for {shipment.RouteDate:MM-dd-yyyy} was not fount";
                        }

                        routeAssignment = await _routeAssignmentProvider.InsertAsync(routeAssignment);

                        success = success || (routeAssignment.Id > 0 && routeAssignment.IsActive);
                    }
                    catch (Exception ex)
                    {
                        _logger.LogError(ex, $"Unhandled exception in {nameof(AssignRoutePlansAsync)}");
                    }
                }
            }

            return success;
        }
    }
}
