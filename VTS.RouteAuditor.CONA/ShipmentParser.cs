﻿using CsvHelper;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.CONA.Models;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.CONA
{
    [Injectable]
    internal class ShipmentParser : IParseData<IEnumerable<IShipment>>
    {
        private readonly Regex _shipmentFileName = new Regex(@"^shipment(?<Number>.*)?(?<Year>[\d]{4})(?<Month>[\d]{2})(?<Day>[\d]{2})\.csv$", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public Task<bool> CanParseAsync(string dataIdentifier, byte[] data = null)
        {
            var canParse = _shipmentFileName.IsMatch(dataIdentifier);
            return Task.FromResult(canParse);
        }

        public Task<IEnumerable<IShipment>> ParseAsync(string dataIdentifier, byte[] data)
        {
            var routeDate = DateTime.MinValue;
            var match = _shipmentFileName.Match(dataIdentifier);
            if (match.Success &&
                int.TryParse(match.Groups["Year"].Value, out var year) &&
                int.TryParse(match.Groups["Month"].Value, out var month) &&
                int.TryParse(match.Groups["Day"].Value, out var day))
                routeDate = new DateTime(year, month, day);

            using (TextReader reader = new StreamReader(new MemoryStream(data)))
            {
                var csvReader = new CsvReader(reader, CultureInfo.InvariantCulture);
                var records = csvReader.GetRecords<Shipment>().Where(s => !s.DriverIdentifier.StartsWith("9")).ToList();

                records.ForEach(r => r.SetRouteDate(routeDate));

                return Task.FromResult<IEnumerable<IShipment>>(records);
            }
        }
    }
}
