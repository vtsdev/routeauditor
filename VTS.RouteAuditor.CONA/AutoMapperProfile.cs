﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Text;
using VTS.RouteAuditor.CONA.Models;

namespace VTS.RouteAuditor.CONA
{
    class AutoMapperProfile : Profile
    {
        public AutoMapperProfile()
        {
            CreateMap<RoutePlan, Common.Models.RoutePlan>()
                .ForMember(dst => dst.RouteIdentifier, opt => opt.MapFrom(src => src.PlanHeader.LEOFixedRoute))
                .ForMember(dst => dst.RouteDate, opt => opt.MapFrom(src => src.PlanHeader.StartDateRoute));

            CreateMap<RoutePlan, Common.Models.TourPlan>()
                .ForMember(dst => dst.TourIdentifier, opt => opt.MapFrom(src => src.PlanHeader.TourID))
                .ForMember(dst => dst.TourSequence, opt => opt.MapFrom(src => ParseTourSequence(src.PlanHeader.TourSequence)))
                .ForMember(dst => dst.ShipmentNumber, opt => opt.MapFrom(src => src.ShipmentHeader.ShipmentNumber))
                .ForMember(dst => dst.StartTime, opt => opt.MapFrom(src => src.PlanHeader.StartTimeRoute.TimeOfDay))
                .ForMember(dst => dst.EndTime, opt => opt.MapFrom(src => src.PlanHeader.EndTimeRoute.TimeOfDay))
                .ForMember(dst => dst.DriveTime, opt => opt.MapFrom(src => ParseTimespan(src.PlanHeader.TotalDriveTimeRoute)))
                .ForMember(dst => dst.WaitTime, opt => opt.MapFrom(src => ParseTimespan(src.PlanHeader.WaitTimeRoute)))
                .ForMember(dst => dst.WorkTime, opt => opt.MapFrom(src => ParseTimespan(src.PlanHeader.WorkTimeRoute)))
                .ForMember(dst => dst.Distance, opt => opt.MapFrom(src => ParseDistance(src.PlanHeader.TotalDistanceRoute)))
                .ForMember(dst => dst.PlannedStops, opt => opt.MapFrom(src => src.Items));

            CreateMap<Item, Common.Models.PlannedStop>()
                .ForMember(dst => dst.TaskId, opt => opt.MapFrom(src => src.TaskID))
                .ForMember(dst => dst.Name, opt => opt.MapFrom(src => IsNull(src.Name1, $"Unnamed ({src.MapReferenceIdentifier})")))
                .ForMember(dst => dst.StopNumber, opt => opt.MapFrom(src => ParseStopNumber(src.RouteItem)))
                .ForMember(dst => dst.CustomerNumber, opt => opt.MapFrom(src => src.CustomerNumber))
                .ForMember(dst => dst.ArrivalTime, opt => opt.MapFrom(src => src.ArrivalTime.TimeOfDay))
                .ForMember(dst => dst.DepartureTime, opt => opt.MapFrom(src => src.DepartureTime.TimeOfDay))
                .ForMember(dst => dst.WorkTime, opt => opt.MapFrom(src => ParseTimespan(src.WorkTimeStop)))
                .ForMember(dst => dst.Cubes, opt => opt.MapFrom(src => ParseCubes(src.Size2D)))
                .ForMember(dst => dst.Cases, opt => opt.MapFrom(src => ParseCases(src.Size3D)));
        }

        #region Private Methods
        private TimeSpan ParseTimespan(string input)
        {
            var match = System.Text.RegularExpressions.Regex.Match(input.Trim(), @"^(?<Hours>[0-9]{3})(?<Minutes>[0-9]{2})$");
            if (match.Success &&
                int.TryParse(match.Groups["Hours"].Value, out var hours) &&
                int.TryParse(match.Groups["Minutes"].Value, out var minutes))
            {
                return new TimeSpan(hours, minutes, 0);
            }

            return TimeSpan.Zero;
        }
        private decimal ParseDistance(string input) => decimal.TryParse(input, out var distance) ? distance : 0;
        private int? ParseCases(string input) => double.TryParse(input, out var cases) ? (int)cases : null as int?;
        private int? ParseCubes(string input) => double.TryParse(input, out var cubes) ? (int)(cubes * 100) : null as int?;
        private int ParseStopNumber(string input) => int.TryParse(input, out var stopNum) ? Math.Max(stopNum - 1, 0) : 0;
        private int ParseTourSequence(string input) => int.TryParse(input, out var tourSequence) ? tourSequence : 1;
        private string IsNull(string input1, string input2)
        {
            if (string.IsNullOrEmpty(input1)) return input2;

            return input1;
        }
        #endregion
    }
}
