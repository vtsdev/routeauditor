﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Serialization;
using VTS.RouteAuditor.Common.Interfaces;
using VTS.RouteAuditor.CONA.Models;
using VTS.RouteAutitor.Common.DependencyInjection;

namespace VTS.RouteAuditor.CONA
{
    [Injectable]
    internal class RouteParser : IParseData<Common.Models.RoutePlan>
    {
        private readonly Regex _routePlanFileName = new Regex(@"^[^_]+_[0-9]{8}-[0-9]{6}-[0-9]{3}.*", RegexOptions.Compiled);

        private readonly ILogger _logger;
        public RouteParser(ILogger<RouteParser> logger)
        {
            _logger = logger;
        }

        public Task<bool> CanParseAsync(string dataIdentifier, byte[] data = null)
        {
            var canParse = _routePlanFileName.IsMatch(dataIdentifier);
            return Task.FromResult(canParse);
        }

        public Task<Common.Models.RoutePlan> ParseAsync(string dataIdentifier, byte[] data)
        {
            try
            {
                var strXml = Encoding.UTF8.GetString(data);
                var serializer = new XmlSerializer(typeof(RoutePlan));
                using (var reader = new StringReader(strXml))
                {
                    var routePlan = serializer.Deserialize(reader) as RoutePlan;
                    if (routePlan != null)
                    {
                        var retVal = Common.ModelMapper.Instance.Map<Common.Models.RoutePlan>(routePlan);
                        var tour = Common.ModelMapper.Instance.Map<Common.Models.TourPlan>(routePlan);
                        retVal.Tours = new[] { tour };
                        retVal.SourceFile = dataIdentifier;

                        return Task.FromResult(retVal);
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, $"Unhandled exception in {nameof(ParseAsync)} - DataIdentifier: {dataIdentifier}");
            }

            return Task.FromResult<Common.Models.RoutePlan>(default);
        }
    }
}
