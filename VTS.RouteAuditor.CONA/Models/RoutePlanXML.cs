﻿namespace VTS.RouteAuditor.CONA.Models
{

    // NOTE: Generated code may require at least .NET Framework 4.5 or .NET Core/Standard 2.0.
    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true, Namespace = "http://coca-cola.com/ftd/wms/app/RoutePlan")]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "http://coca-cola.com/ftd/wms/app/RoutePlan", IsNullable = false)]
    public partial class RoutePlan
    {

        private PlanHeader planHeaderField;

        private ShipmentHeader shipmentHeaderField;

        private RouteDocument[] routeDocumentsField;

        private ResourceDueList resourceDueListField;

        private Item[] itemsField;

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public PlanHeader PlanHeader
        {
            get
            {
                return this.planHeaderField;
            }
            set
            {
                this.planHeaderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public ShipmentHeader ShipmentHeader
        {
            get
            {
                return this.shipmentHeaderField;
            }
            set
            {
                this.shipmentHeaderField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("RouteDocuments", Namespace = "")]
        public RouteDocument[] RouteDocuments
        {
            get
            {
                return this.routeDocumentsField;
            }
            set
            {
                this.routeDocumentsField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(Namespace = "")]
        public ResourceDueList ResourceDueList
        {
            get
            {
                return this.resourceDueListField;
            }
            set
            {
                this.resourceDueListField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute("Items", Namespace = "")]
        public Item[] Items
        {
            get
            {
                return this.itemsField;
            }
            set
            {
                this.itemsField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class PlanHeader
    {

        private string planIDField;

        private string scenarioIDField;

        private string tourIDField;

        private string tourSequenceField;

        private string shipmentNumberOutField;

        private string shipmentTypeOutField;

        private string shipmentGUIDOutField;

        private string releasedField;

        private string lockCodeField;

        private System.DateTime startDateRouteField;

        private System.DateTime startTimeRouteField;

        private System.DateTime endDateRouteField;

        private System.DateTime endTimeRouteField;

        private string workTimeRouteField;

        private string totalDriveTimeRouteField;

        private string waitTimeRouteField;

        private string totalDistanceRouteField;

        private string totalCostsField;

        private string totalUnloadWeightRouteField;

        private string totalUnloadQty2Field;

        private string totalUnloadQty3Field;

        private string numberOfStopsField;

        private string violationCodeField;

        private string lEOLoadSpaceField;

        private string lEOLoadTypeField;

        private string loadSpaceNetWeightField;

        private string loadSpaceUnitField;

        private string plateField;

        private string resultLevelField;

        private string virutalCostsField;

        private string helperAssignedField;

        private string helperRequiredField;

        private string equipmentNumberField;

        private string actualDriverField;

        private string vehicleNameField;

        private string size1DField;

        private string size2DField;

        private string size3DField;

        private string vehicleTypeField;

        private string nameField;

        private string houseNumberField;

        private string postalCodeField;

        private string cityField;

        private string actualQuantityDeliveredField;

        private string startDepotField;

        private string endDepotField;

        private string fictiveClusterCostsField;

        private string fictiveSameLocationCostsField;

        private string fictiveMinimumCasesField;

        private string totalManualBreaksField;

        private string merchandisingTimeTotalField;

        private string refTourIDField;

        private string merchandisingTimeField;

        private string deliverySize2Field;

        private string deliveryCountField;

        private string stopCountField;

        private System.DateTime timeAtDepotField;

        private string lEOFixedRouteField;

        /// <remarks/>
        public string PlanID
        {
            get
            {
                return this.planIDField;
            }
            set
            {
                this.planIDField = value;
            }
        }

        /// <remarks/>
        public string ScenarioID
        {
            get
            {
                return this.scenarioIDField;
            }
            set
            {
                this.scenarioIDField = value;
            }
        }

        /// <remarks/>
        public string TourID
        {
            get
            {
                return this.tourIDField;
            }
            set
            {
                this.tourIDField = value;
            }
        }

        /// <remarks/>
        public string TourSequence
        {
            get
            {
                return this.tourSequenceField;
            }
            set
            {
                this.tourSequenceField = value;
            }
        }

        /// <remarks/>
        public string ShipmentNumberOut
        {
            get
            {
                return this.shipmentNumberOutField;
            }
            set
            {
                this.shipmentNumberOutField = value;
            }
        }

        /// <remarks/>
        public string ShipmentTypeOut
        {
            get
            {
                return this.shipmentTypeOutField;
            }
            set
            {
                this.shipmentTypeOutField = value;
            }
        }

        /// <remarks/>
        public string ShipmentGUIDOut
        {
            get
            {
                return this.shipmentGUIDOutField;
            }
            set
            {
                this.shipmentGUIDOutField = value;
            }
        }

        /// <remarks/>
        public string Released
        {
            get
            {
                return this.releasedField;
            }
            set
            {
                this.releasedField = value;
            }
        }

        /// <remarks/>
        public string LockCode
        {
            get
            {
                return this.lockCodeField;
            }
            set
            {
                this.lockCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime StartDateRoute
        {
            get
            {
                return this.startDateRouteField;
            }
            set
            {
                this.startDateRouteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime StartTimeRoute
        {
            get
            {
                return this.startTimeRouteField;
            }
            set
            {
                this.startTimeRouteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime EndDateRoute
        {
            get
            {
                return this.endDateRouteField;
            }
            set
            {
                this.endDateRouteField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime EndTimeRoute
        {
            get
            {
                return this.endTimeRouteField;
            }
            set
            {
                this.endTimeRouteField = value;
            }
        }

        /// <remarks/>
        public string WorkTimeRoute
        {
            get
            {
                return this.workTimeRouteField;
            }
            set
            {
                this.workTimeRouteField = value;
            }
        }

        /// <remarks/>
        public string TotalDriveTimeRoute
        {
            get
            {
                return this.totalDriveTimeRouteField;
            }
            set
            {
                this.totalDriveTimeRouteField = value;
            }
        }

        /// <remarks/>
        public string WaitTimeRoute
        {
            get
            {
                return this.waitTimeRouteField;
            }
            set
            {
                this.waitTimeRouteField = value;
            }
        }

        /// <remarks/>
        public string TotalDistanceRoute
        {
            get
            {
                return this.totalDistanceRouteField;
            }
            set
            {
                this.totalDistanceRouteField = value;
            }
        }

        /// <remarks/>
        public string TotalCosts
        {
            get
            {
                return this.totalCostsField;
            }
            set
            {
                this.totalCostsField = value;
            }
        }

        /// <remarks/>
        public string TotalUnloadWeightRoute
        {
            get
            {
                return this.totalUnloadWeightRouteField;
            }
            set
            {
                this.totalUnloadWeightRouteField = value;
            }
        }

        /// <remarks/>
        public string TotalUnloadQty2
        {
            get
            {
                return this.totalUnloadQty2Field;
            }
            set
            {
                this.totalUnloadQty2Field = value;
            }
        }

        /// <remarks/>
        public string TotalUnloadQty3
        {
            get
            {
                return this.totalUnloadQty3Field;
            }
            set
            {
                this.totalUnloadQty3Field = value;
            }
        }

        /// <remarks/>
        public string NumberOfStops
        {
            get
            {
                return this.numberOfStopsField;
            }
            set
            {
                this.numberOfStopsField = value;
            }
        }

        /// <remarks/>
        public string ViolationCode
        {
            get
            {
                return this.violationCodeField;
            }
            set
            {
                this.violationCodeField = value;
            }
        }

        /// <remarks/>
        public string LEOLoadSpace
        {
            get
            {
                return this.lEOLoadSpaceField;
            }
            set
            {
                this.lEOLoadSpaceField = value;
            }
        }

        /// <remarks/>
        public string LEOLoadType
        {
            get
            {
                return this.lEOLoadTypeField;
            }
            set
            {
                this.lEOLoadTypeField = value;
            }
        }

        /// <remarks/>
        public string LoadSpaceNetWeight
        {
            get
            {
                return this.loadSpaceNetWeightField;
            }
            set
            {
                this.loadSpaceNetWeightField = value;
            }
        }

        /// <remarks/>
        public string LoadSpaceUnit
        {
            get
            {
                return this.loadSpaceUnitField;
            }
            set
            {
                this.loadSpaceUnitField = value;
            }
        }

        /// <remarks/>
        public string Plate
        {
            get
            {
                return this.plateField;
            }
            set
            {
                this.plateField = value;
            }
        }

        /// <remarks/>
        public string ResultLevel
        {
            get
            {
                return this.resultLevelField;
            }
            set
            {
                this.resultLevelField = value;
            }
        }

        /// <remarks/>
        public string VirutalCosts
        {
            get
            {
                return this.virutalCostsField;
            }
            set
            {
                this.virutalCostsField = value;
            }
        }

        /// <remarks/>
        public string HelperAssigned
        {
            get
            {
                return this.helperAssignedField;
            }
            set
            {
                this.helperAssignedField = value;
            }
        }

        /// <remarks/>
        public string HelperRequired
        {
            get
            {
                return this.helperRequiredField;
            }
            set
            {
                this.helperRequiredField = value;
            }
        }

        /// <remarks/>
        public string EquipmentNumber
        {
            get
            {
                return this.equipmentNumberField;
            }
            set
            {
                this.equipmentNumberField = value;
            }
        }

        /// <remarks/>
        public string ActualDriver
        {
            get
            {
                return this.actualDriverField;
            }
            set
            {
                this.actualDriverField = value;
            }
        }

        /// <remarks/>
        public string VehicleName
        {
            get
            {
                return this.vehicleNameField;
            }
            set
            {
                this.vehicleNameField = value;
            }
        }

        /// <remarks/>
        public string Size1D
        {
            get
            {
                return this.size1DField;
            }
            set
            {
                this.size1DField = value;
            }
        }

        /// <remarks/>
        public string Size2D
        {
            get
            {
                return this.size2DField;
            }
            set
            {
                this.size2DField = value;
            }
        }

        /// <remarks/>
        public string Size3D
        {
            get
            {
                return this.size3DField;
            }
            set
            {
                this.size3DField = value;
            }
        }

        /// <remarks/>
        public string VehicleType
        {
            get
            {
                return this.vehicleTypeField;
            }
            set
            {
                this.vehicleTypeField = value;
            }
        }

        /// <remarks/>
        public string Name
        {
            get
            {
                return this.nameField;
            }
            set
            {
                this.nameField = value;
            }
        }

        /// <remarks/>
        public string HouseNumber
        {
            get
            {
                return this.houseNumberField;
            }
            set
            {
                this.houseNumberField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string ActualQuantityDelivered
        {
            get
            {
                return this.actualQuantityDeliveredField;
            }
            set
            {
                this.actualQuantityDeliveredField = value;
            }
        }

        /// <remarks/>
        public string StartDepot
        {
            get
            {
                return this.startDepotField;
            }
            set
            {
                this.startDepotField = value;
            }
        }

        /// <remarks/>
        public string EndDepot
        {
            get
            {
                return this.endDepotField;
            }
            set
            {
                this.endDepotField = value;
            }
        }

        /// <remarks/>
        public string FictiveClusterCosts
        {
            get
            {
                return this.fictiveClusterCostsField;
            }
            set
            {
                this.fictiveClusterCostsField = value;
            }
        }

        /// <remarks/>
        public string FictiveSameLocationCosts
        {
            get
            {
                return this.fictiveSameLocationCostsField;
            }
            set
            {
                this.fictiveSameLocationCostsField = value;
            }
        }

        /// <remarks/>
        public string FictiveMinimumCases
        {
            get
            {
                return this.fictiveMinimumCasesField;
            }
            set
            {
                this.fictiveMinimumCasesField = value;
            }
        }

        /// <remarks/>
        public string TotalManualBreaks
        {
            get
            {
                return this.totalManualBreaksField;
            }
            set
            {
                this.totalManualBreaksField = value;
            }
        }

        /// <remarks/>
        public string MerchandisingTimeTotal
        {
            get
            {
                return this.merchandisingTimeTotalField;
            }
            set
            {
                this.merchandisingTimeTotalField = value;
            }
        }

        /// <remarks/>
        public string RefTourID
        {
            get
            {
                return this.refTourIDField;
            }
            set
            {
                this.refTourIDField = value;
            }
        }

        /// <remarks/>
        public string MerchandisingTime
        {
            get
            {
                return this.merchandisingTimeField;
            }
            set
            {
                this.merchandisingTimeField = value;
            }
        }

        /// <remarks/>
        public string DeliverySize2
        {
            get
            {
                return this.deliverySize2Field;
            }
            set
            {
                this.deliverySize2Field = value;
            }
        }

        /// <remarks/>
        public string DeliveryCount
        {
            get
            {
                return this.deliveryCountField;
            }
            set
            {
                this.deliveryCountField = value;
            }
        }

        /// <remarks/>
        public string StopCount
        {
            get
            {
                return this.stopCountField;
            }
            set
            {
                this.stopCountField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime TimeAtDepot
        {
            get
            {
                return this.timeAtDepotField;
            }
            set
            {
                this.timeAtDepotField = value;
            }
        }

        /// <remarks/>
        public string LEOFixedRoute
        {
            get
            {
                return this.lEOFixedRouteField;
            }
            set
            {
                this.lEOFixedRouteField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ShipmentHeader
    {

        private string shipmentNumberField;

        private string documentCategoryField;

        private string shipmentType1Field;

        private string transportationPlanningPostringField;

        private string personNameCreatorField;

        private System.DateTime recordCreationDateField;

        private System.DateTime entryTimeField;

        private string personNameChangeField;

        private System.DateTime changedOnField;

        private string timeLastChangeField;

        private string shipmentCompletitionTypeField;

        private string processingControlField;

        private string serviceTypeField;

        private string shipmentType2Field;

        private string legIndicatorField;

        private string shiipmentRouteField;

        private string shipmentDescriptionField;

        private string transportationStatusField;

        private System.DateTime planningEndDateField;

        private System.DateTime schedulingEndTimeField;

        private System.DateTime checkPlannedDateField;

        private System.DateTime checkPlannedTimeField;

        private System.DateTime loadStatusPlannedDateField;

        private System.DateTime loadStatusStartTimeField;

        private System.DateTime loadEndPlannedDateField;

        private System.DateTime loadEndPlannedTimeField;

        private System.DateTime shipmentPlannedDateCField;

        private System.DateTime shipmentPlannedTimeField;

        private string transportationTimeField;

        private System.DateTime shipmentPlanneddateField;

        private System.DateTime transportStartTimeField;

        private System.DateTime shipmentEndPlannedDateField;

        private System.DateTime plannedTransportEndTimeField;

        private string overallTransportationStatusField;

        private string transportationWeightUnitField;

        private string transportationVolumeField;

        private string distanceField;

        private string distanceUOMField;

        private string travellingTimeUOMField;

        private string transportationTotalTimeField;

        private string transportationDurationField;

        private string deliveryRouteField;

        private string globallyUniqueIdentifierField;

        private string timeSegmentGroupField;

        private string add01Field;

        private string dangerousGoodsSelectionField;

        private string shipmentMaximumPriceField;

        private string shipmentActualCostsField;

        private string sequenceofTransportsField;

        private string driver1Field;

        private string vehicleField;

        private string trailerField;

        private string loadingSequenceNumberField;

        private string lEOPlanField;

        private string shippingPostringField;

        private string distributionModeField;

        private string lEORouteField;

        private string lEOFixedRouteField;

        private string pTSStatusField;

        /// <remarks/>
        public string ShipmentNumber
        {
            get
            {
                return this.shipmentNumberField;
            }
            set
            {
                this.shipmentNumberField = value;
            }
        }

        /// <remarks/>
        public string DocumentCategory
        {
            get
            {
                return this.documentCategoryField;
            }
            set
            {
                this.documentCategoryField = value;
            }
        }

        /// <remarks/>
        public string ShipmentType1
        {
            get
            {
                return this.shipmentType1Field;
            }
            set
            {
                this.shipmentType1Field = value;
            }
        }

        /// <remarks/>
        public string TransportationPlanningPostring
        {
            get
            {
                return this.transportationPlanningPostringField;
            }
            set
            {
                this.transportationPlanningPostringField = value;
            }
        }

        /// <remarks/>
        public string PersonNameCreator
        {
            get
            {
                return this.personNameCreatorField;
            }
            set
            {
                this.personNameCreatorField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime RecordCreationDate
        {
            get
            {
                return this.recordCreationDateField;
            }
            set
            {
                this.recordCreationDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime EntryTime
        {
            get
            {
                return this.entryTimeField;
            }
            set
            {
                this.entryTimeField = value;
            }
        }

        /// <remarks/>
        public string PersonNameChange
        {
            get
            {
                return this.personNameChangeField;
            }
            set
            {
                this.personNameChangeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime ChangedOn
        {
            get
            {
                return this.changedOnField;
            }
            set
            {
                this.changedOnField = value;
            }
        }

        /// <remarks/>
        public string TimeLastChange
        {
            get
            {
                return this.timeLastChangeField;
            }
            set
            {
                this.timeLastChangeField = value;
            }
        }

        /// <remarks/>
        public string ShipmentCompletitionType
        {
            get
            {
                return this.shipmentCompletitionTypeField;
            }
            set
            {
                this.shipmentCompletitionTypeField = value;
            }
        }

        /// <remarks/>
        public string ProcessingControl
        {
            get
            {
                return this.processingControlField;
            }
            set
            {
                this.processingControlField = value;
            }
        }

        /// <remarks/>
        public string ServiceType
        {
            get
            {
                return this.serviceTypeField;
            }
            set
            {
                this.serviceTypeField = value;
            }
        }

        /// <remarks/>
        public string ShipmentType2
        {
            get
            {
                return this.shipmentType2Field;
            }
            set
            {
                this.shipmentType2Field = value;
            }
        }

        /// <remarks/>
        public string LegIndicator
        {
            get
            {
                return this.legIndicatorField;
            }
            set
            {
                this.legIndicatorField = value;
            }
        }

        /// <remarks/>
        public string ShiipmentRoute
        {
            get
            {
                return this.shiipmentRouteField;
            }
            set
            {
                this.shiipmentRouteField = value;
            }
        }

        /// <remarks/>
        public string ShipmentDescription
        {
            get
            {
                return this.shipmentDescriptionField;
            }
            set
            {
                this.shipmentDescriptionField = value;
            }
        }

        /// <remarks/>
        public string TransportationStatus
        {
            get
            {
                return this.transportationStatusField;
            }
            set
            {
                this.transportationStatusField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime PlanningEndDate
        {
            get
            {
                return this.planningEndDateField;
            }
            set
            {
                this.planningEndDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime SchedulingEndTime
        {
            get
            {
                return this.schedulingEndTimeField;
            }
            set
            {
                this.schedulingEndTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime CheckPlannedDate
        {
            get
            {
                return this.checkPlannedDateField;
            }
            set
            {
                this.checkPlannedDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime CheckPlannedTime
        {
            get
            {
                return this.checkPlannedTimeField;
            }
            set
            {
                this.checkPlannedTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime LoadStatusPlannedDate
        {
            get
            {
                return this.loadStatusPlannedDateField;
            }
            set
            {
                this.loadStatusPlannedDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime LoadStatusStartTime
        {
            get
            {
                return this.loadStatusStartTimeField;
            }
            set
            {
                this.loadStatusStartTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime LoadEndPlannedDate
        {
            get
            {
                return this.loadEndPlannedDateField;
            }
            set
            {
                this.loadEndPlannedDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime LoadEndPlannedTime
        {
            get
            {
                return this.loadEndPlannedTimeField;
            }
            set
            {
                this.loadEndPlannedTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime ShipmentPlannedDateC
        {
            get
            {
                return this.shipmentPlannedDateCField;
            }
            set
            {
                this.shipmentPlannedDateCField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime ShipmentPlannedTime
        {
            get
            {
                return this.shipmentPlannedTimeField;
            }
            set
            {
                this.shipmentPlannedTimeField = value;
            }
        }

        /// <remarks/>
        public string TransportationTime
        {
            get
            {
                return this.transportationTimeField;
            }
            set
            {
                this.transportationTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime ShipmentPlanneddate
        {
            get
            {
                return this.shipmentPlanneddateField;
            }
            set
            {
                this.shipmentPlanneddateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime TransportStartTime
        {
            get
            {
                return this.transportStartTimeField;
            }
            set
            {
                this.transportStartTimeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime ShipmentEndPlannedDate
        {
            get
            {
                return this.shipmentEndPlannedDateField;
            }
            set
            {
                this.shipmentEndPlannedDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime PlannedTransportEndTime
        {
            get
            {
                return this.plannedTransportEndTimeField;
            }
            set
            {
                this.plannedTransportEndTimeField = value;
            }
        }

        /// <remarks/>
        public string OverallTransportationStatus
        {
            get
            {
                return this.overallTransportationStatusField;
            }
            set
            {
                this.overallTransportationStatusField = value;
            }
        }

        /// <remarks/>
        public string TransportationWeightUnit
        {
            get
            {
                return this.transportationWeightUnitField;
            }
            set
            {
                this.transportationWeightUnitField = value;
            }
        }

        /// <remarks/>
        public string TransportationVolume
        {
            get
            {
                return this.transportationVolumeField;
            }
            set
            {
                this.transportationVolumeField = value;
            }
        }

        /// <remarks/>
        public string Distance
        {
            get
            {
                return this.distanceField;
            }
            set
            {
                this.distanceField = value;
            }
        }

        /// <remarks/>
        public string DistanceUOM
        {
            get
            {
                return this.distanceUOMField;
            }
            set
            {
                this.distanceUOMField = value;
            }
        }

        /// <remarks/>
        public string TravellingTimeUOM
        {
            get
            {
                return this.travellingTimeUOMField;
            }
            set
            {
                this.travellingTimeUOMField = value;
            }
        }

        /// <remarks/>
        public string TransportationTotalTime
        {
            get
            {
                return this.transportationTotalTimeField;
            }
            set
            {
                this.transportationTotalTimeField = value;
            }
        }

        /// <remarks/>
        public string TransportationDuration
        {
            get
            {
                return this.transportationDurationField;
            }
            set
            {
                this.transportationDurationField = value;
            }
        }

        /// <remarks/>
        public string DeliveryRoute
        {
            get
            {
                return this.deliveryRouteField;
            }
            set
            {
                this.deliveryRouteField = value;
            }
        }

        /// <remarks/>
        public string GloballyUniqueIdentifier
        {
            get
            {
                return this.globallyUniqueIdentifierField;
            }
            set
            {
                this.globallyUniqueIdentifierField = value;
            }
        }

        /// <remarks/>
        public string TimeSegmentGroup
        {
            get
            {
                return this.timeSegmentGroupField;
            }
            set
            {
                this.timeSegmentGroupField = value;
            }
        }

        /// <remarks/>
        public string Add01
        {
            get
            {
                return this.add01Field;
            }
            set
            {
                this.add01Field = value;
            }
        }

        /// <remarks/>
        public string DangerousGoodsSelection
        {
            get
            {
                return this.dangerousGoodsSelectionField;
            }
            set
            {
                this.dangerousGoodsSelectionField = value;
            }
        }

        /// <remarks/>
        public string ShipmentMaximumPrice
        {
            get
            {
                return this.shipmentMaximumPriceField;
            }
            set
            {
                this.shipmentMaximumPriceField = value;
            }
        }

        /// <remarks/>
        public string ShipmentActualCosts
        {
            get
            {
                return this.shipmentActualCostsField;
            }
            set
            {
                this.shipmentActualCostsField = value;
            }
        }

        /// <remarks/>
        public string SequenceofTransports
        {
            get
            {
                return this.sequenceofTransportsField;
            }
            set
            {
                this.sequenceofTransportsField = value;
            }
        }

        /// <remarks/>
        public string Driver1
        {
            get
            {
                return this.driver1Field;
            }
            set
            {
                this.driver1Field = value;
            }
        }

        /// <remarks/>
        public string Vehicle
        {
            get
            {
                return this.vehicleField;
            }
            set
            {
                this.vehicleField = value;
            }
        }

        /// <remarks/>
        public string Trailer
        {
            get
            {
                return this.trailerField;
            }
            set
            {
                this.trailerField = value;
            }
        }

        /// <remarks/>
        public string LoadingSequenceNumber
        {
            get
            {
                return this.loadingSequenceNumberField;
            }
            set
            {
                this.loadingSequenceNumberField = value;
            }
        }

        /// <remarks/>
        public string LEOPlan
        {
            get
            {
                return this.lEOPlanField;
            }
            set
            {
                this.lEOPlanField = value;
            }
        }

        /// <remarks/>
        public string ShippingPostring
        {
            get
            {
                return this.shippingPostringField;
            }
            set
            {
                this.shippingPostringField = value;
            }
        }

        /// <remarks/>
        public string DistributionMode
        {
            get
            {
                return this.distributionModeField;
            }
            set
            {
                this.distributionModeField = value;
            }
        }

        /// <remarks/>
        public string LEORoute
        {
            get
            {
                return this.lEORouteField;
            }
            set
            {
                this.lEORouteField = value;
            }
        }

        /// <remarks/>
        public string LEOFixedRoute
        {
            get
            {
                return this.lEOFixedRouteField;
            }
            set
            {
                this.lEOFixedRouteField = value;
            }
        }

        /// <remarks/>
        public string PTSStatus
        {
            get
            {
                return this.pTSStatusField;
            }
            set
            {
                this.pTSStatusField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class RouteDocument
    {

        private string planIDField;

        private string scenarioIDField;

        private string tourIDField;

        private string tourSequenceField;

        private string routeItemField;

        private string counterField;

        private string documentCategoryField;

        private string documentNumberField;

        private string shippingOrderCategoryField;

        private string shippingOrderField;

        private string consignmentField;

        private System.DateTime timeStampField;

        private string lockcodeField;

        private string mapReferenceIdentifierField;

        private string arrivalDateStopField;

        private System.DateTime arrivalTimeField;

        private string realisedArrivalDateStopField;

        private System.DateTime realisedArrivalTimeField;

        private string departureDateStopField;

        private System.DateTime departureTimeField;

        private string realisedDepartureDateStopField;

        private System.DateTime realisedDepartureTimeField;

        private string waitTimeStopField;

        private string driveTimeField;

        private string workTimeStopField;

        private string distanceField;

        private bool distanceFieldSpecified;

        private string totalUnloadWeightField;

        private string totalUnloadQtySize2Field;

        private string totalUnloadQtySize3Field;

        private string violationCodeField;

        private string partyField;

        private string size1DField;

        private string size2DField;

        private string size3DField;

        private string additionalCustomerNumberField;

        private string lEOPositionInLoadSpaceField;

        private string detachField;

        private string attachField;

        private string extraDrivingField;

        private string extraDrivingSumField;

        private string waitingTimeField;

        private string breakDurationField;

        private string merchandisingTimeField;

        private string lEOFixedRouteField;

        /// <remarks/>
        public string PlanID
        {
            get
            {
                return this.planIDField;
            }
            set
            {
                this.planIDField = value;
            }
        }

        /// <remarks/>
        public string ScenarioID
        {
            get
            {
                return this.scenarioIDField;
            }
            set
            {
                this.scenarioIDField = value;
            }
        }

        /// <remarks/>
        public string TourID
        {
            get
            {
                return this.tourIDField;
            }
            set
            {
                this.tourIDField = value;
            }
        }

        /// <remarks/>
        public string TourSequence
        {
            get
            {
                return this.tourSequenceField;
            }
            set
            {
                this.tourSequenceField = value;
            }
        }

        /// <remarks/>
        public string RouteItem
        {
            get
            {
                return this.routeItemField;
            }
            set
            {
                this.routeItemField = value;
            }
        }

        /// <remarks/>
        public string Counter
        {
            get
            {
                return this.counterField;
            }
            set
            {
                this.counterField = value;
            }
        }

        /// <remarks/>
        public string DocumentCategory
        {
            get
            {
                return this.documentCategoryField;
            }
            set
            {
                this.documentCategoryField = value;
            }
        }

        /// <remarks/>
        public string DocumentNumber
        {
            get
            {
                return this.documentNumberField;
            }
            set
            {
                this.documentNumberField = value;
            }
        }

        /// <remarks/>
        public string ShippingOrderCategory
        {
            get
            {
                return this.shippingOrderCategoryField;
            }
            set
            {
                this.shippingOrderCategoryField = value;
            }
        }

        /// <remarks/>
        public string ShippingOrder
        {
            get
            {
                return this.shippingOrderField;
            }
            set
            {
                this.shippingOrderField = value;
            }
        }

        /// <remarks/>
        public string Consignment
        {
            get
            {
                return this.consignmentField;
            }
            set
            {
                this.consignmentField = value;
            }
        }

        /// <remarks/>
        public System.DateTime TimeStamp
        {
            get
            {
                return this.timeStampField;
            }
            set
            {
                this.timeStampField = value;
            }
        }

        /// <remarks/>
        public string Lockcode
        {
            get
            {
                return this.lockcodeField;
            }
            set
            {
                this.lockcodeField = value;
            }
        }

        /// <remarks/>
        public string MapReferenceIdentifier
        {
            get
            {
                return this.mapReferenceIdentifierField;
            }
            set
            {
                this.mapReferenceIdentifierField = value;
            }
        }

        /// <remarks/>
        public string ArrivalDateStop
        {
            get
            {
                return this.arrivalDateStopField;
            }
            set
            {
                this.arrivalDateStopField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime ArrivalTime
        {
            get
            {
                return this.arrivalTimeField;
            }
            set
            {
                this.arrivalTimeField = value;
            }
        }

        /// <remarks/>
        public string RealisedArrivalDateStop
        {
            get
            {
                return this.realisedArrivalDateStopField;
            }
            set
            {
                this.realisedArrivalDateStopField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime RealisedArrivalTime
        {
            get
            {
                return this.realisedArrivalTimeField;
            }
            set
            {
                this.realisedArrivalTimeField = value;
            }
        }

        /// <remarks/>
        public string DepartureDateStop
        {
            get
            {
                return this.departureDateStopField;
            }
            set
            {
                this.departureDateStopField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime DepartureTime
        {
            get
            {
                return this.departureTimeField;
            }
            set
            {
                this.departureTimeField = value;
            }
        }

        /// <remarks/>
        public string RealisedDepartureDateStop
        {
            get
            {
                return this.realisedDepartureDateStopField;
            }
            set
            {
                this.realisedDepartureDateStopField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime RealisedDepartureTime
        {
            get
            {
                return this.realisedDepartureTimeField;
            }
            set
            {
                this.realisedDepartureTimeField = value;
            }
        }

        /// <remarks/>
        public string WaitTimeStop
        {
            get
            {
                return this.waitTimeStopField;
            }
            set
            {
                this.waitTimeStopField = value;
            }
        }

        /// <remarks/>
        public string DriveTime
        {
            get
            {
                return this.driveTimeField;
            }
            set
            {
                this.driveTimeField = value;
            }
        }

        /// <remarks/>
        public string WorkTimeStop
        {
            get
            {
                return this.workTimeStopField;
            }
            set
            {
                this.workTimeStopField = value;
            }
        }

        /// <remarks/>
        public string Distance
        {
            get
            {
                return this.distanceField;
            }
            set
            {
                this.distanceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DistanceSpecified
        {
            get
            {
                return this.distanceFieldSpecified;
            }
            set
            {
                this.distanceFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string TotalUnloadWeight
        {
            get
            {
                return this.totalUnloadWeightField;
            }
            set
            {
                this.totalUnloadWeightField = value;
            }
        }

        /// <remarks/>
        public string TotalUnloadQtySize2
        {
            get
            {
                return this.totalUnloadQtySize2Field;
            }
            set
            {
                this.totalUnloadQtySize2Field = value;
            }
        }

        /// <remarks/>
        public string TotalUnloadQtySize3
        {
            get
            {
                return this.totalUnloadQtySize3Field;
            }
            set
            {
                this.totalUnloadQtySize3Field = value;
            }
        }

        /// <remarks/>
        public string ViolationCode
        {
            get
            {
                return this.violationCodeField;
            }
            set
            {
                this.violationCodeField = value;
            }
        }

        /// <remarks/>
        public string Party
        {
            get
            {
                return this.partyField;
            }
            set
            {
                this.partyField = value;
            }
        }

        /// <remarks/>
        public string Size1D
        {
            get
            {
                return this.size1DField;
            }
            set
            {
                this.size1DField = value;
            }
        }

        /// <remarks/>
        public string Size2D
        {
            get
            {
                return this.size2DField;
            }
            set
            {
                this.size2DField = value;
            }
        }

        /// <remarks/>
        public string Size3D
        {
            get
            {
                return this.size3DField;
            }
            set
            {
                this.size3DField = value;
            }
        }

        /// <remarks/>
        public string AdditionalCustomerNumber
        {
            get
            {
                return this.additionalCustomerNumberField;
            }
            set
            {
                this.additionalCustomerNumberField = value;
            }
        }

        /// <remarks/>
        public string LEOPositionInLoadSpace
        {
            get
            {
                return this.lEOPositionInLoadSpaceField;
            }
            set
            {
                this.lEOPositionInLoadSpaceField = value;
            }
        }

        /// <remarks/>
        public string Detach
        {
            get
            {
                return this.detachField;
            }
            set
            {
                this.detachField = value;
            }
        }

        /// <remarks/>
        public string Attach
        {
            get
            {
                return this.attachField;
            }
            set
            {
                this.attachField = value;
            }
        }

        /// <remarks/>
        public string ExtraDriving
        {
            get
            {
                return this.extraDrivingField;
            }
            set
            {
                this.extraDrivingField = value;
            }
        }

        /// <remarks/>
        public string ExtraDrivingSum
        {
            get
            {
                return this.extraDrivingSumField;
            }
            set
            {
                this.extraDrivingSumField = value;
            }
        }

        /// <remarks/>
        public string WaitingTime
        {
            get
            {
                return this.waitingTimeField;
            }
            set
            {
                this.waitingTimeField = value;
            }
        }

        /// <remarks/>
        public string BreakDuration
        {
            get
            {
                return this.breakDurationField;
            }
            set
            {
                this.breakDurationField = value;
            }
        }

        /// <remarks/>
        public string MerchandisingTime
        {
            get
            {
                return this.merchandisingTimeField;
            }
            set
            {
                this.merchandisingTimeField = value;
            }
        }

        /// <remarks/>
        public string LEOFixedRoute
        {
            get
            {
                return this.lEOFixedRouteField;
            }
            set
            {
                this.lEOFixedRouteField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class ResourceDueList
    {

        private string lEOPlanField;

        private string lEOPlanScenarioField;

        private string lEOLoadSpaceField;

        private string lEOLoadSpaceTypeField;

        private string loadSpaceNetWeightField;

        private string lEOLoadSpaceCombinationField;

        private string maxQuantityLoadSpaceField;

        private string tractorField;

        private string driverField;

        private string assignedField;

        private string assignedQuantityField;

        private string fixationLockCodeField;

        private System.DateTime startDateLoadSpaceField;

        private System.DateTime startTimeLoadSpaceField;

        private System.DateTime combinationPlanningEndDateField;

        private System.DateTime combinationPlanningEndTimeField;

        private string combinationPlanningWorktimeField;

        private string combinationPlanningDrivingTimeField;

        private string combinationPlanningWaitingTimeField;

        private string combinationPlanningDistanceField;

        private string combinationPlanningTotalCostsField;

        private string numberOfRoutesField;

        private string violationCodeField;

        private string referenceLEOPlanScenarioField;

        private string referenceRouteIDField;

        private string referenceRouteSequenceNumberField;

        private string licensePlateNumberField;

        private string lEOFicticiousCostsField;

        private string helperAssignedField;

        private string vehicleNameField;

        private string equipmentNumberField;

        private string resourcesSize1Field;

        private string resourcesSize2Field;

        private string resourcesSize3Field;

        private string lEODepotIDField;

        private string transportationPlanningPostringField;

        private string explanationCodeField;

        private string vehicleTypeField;

        private string vehicleDescriptionField;

        private string returnToDepotField;

        private string distributionModeField;

        private string forklitOnBoardField;

        private string fixTimeBeforeTripField;

        private string fixReloadTimeField;

        private string fixTimeAfterTripField;

        private string fixedTimeUnitField;

        private string fictiveClusterCostsField;

        private string fictiveLocationCostsField;

        private string fictiveMinimumCasesField;

        private string totalWeight1Field;

        private string driverProductivityField;

        private string cO2RackTypeField;

        private string thresholdMiniPalletsField;

        private string thresholdSuperMiniPalletsField;

        private string distributionMode2Field;

        private string durationBreakTimeNewField;

        /// <remarks/>
        public string LEOPlan
        {
            get
            {
                return this.lEOPlanField;
            }
            set
            {
                this.lEOPlanField = value;
            }
        }

        /// <remarks/>
        public string LEOPlanScenario
        {
            get
            {
                return this.lEOPlanScenarioField;
            }
            set
            {
                this.lEOPlanScenarioField = value;
            }
        }

        /// <remarks/>
        public string LEOLoadSpace
        {
            get
            {
                return this.lEOLoadSpaceField;
            }
            set
            {
                this.lEOLoadSpaceField = value;
            }
        }

        /// <remarks/>
        public string LEOLoadSpaceType
        {
            get
            {
                return this.lEOLoadSpaceTypeField;
            }
            set
            {
                this.lEOLoadSpaceTypeField = value;
            }
        }

        /// <remarks/>
        public string LoadSpaceNetWeight
        {
            get
            {
                return this.loadSpaceNetWeightField;
            }
            set
            {
                this.loadSpaceNetWeightField = value;
            }
        }

        /// <remarks/>
        public string LEOLoadSpaceCombination
        {
            get
            {
                return this.lEOLoadSpaceCombinationField;
            }
            set
            {
                this.lEOLoadSpaceCombinationField = value;
            }
        }

        /// <remarks/>
        public string MaxQuantityLoadSpace
        {
            get
            {
                return this.maxQuantityLoadSpaceField;
            }
            set
            {
                this.maxQuantityLoadSpaceField = value;
            }
        }

        /// <remarks/>
        public string Tractor
        {
            get
            {
                return this.tractorField;
            }
            set
            {
                this.tractorField = value;
            }
        }

        /// <remarks/>
        public string Driver
        {
            get
            {
                return this.driverField;
            }
            set
            {
                this.driverField = value;
            }
        }

        /// <remarks/>
        public string Assigned
        {
            get
            {
                return this.assignedField;
            }
            set
            {
                this.assignedField = value;
            }
        }

        /// <remarks/>
        public string AssignedQuantity
        {
            get
            {
                return this.assignedQuantityField;
            }
            set
            {
                this.assignedQuantityField = value;
            }
        }

        /// <remarks/>
        public string FixationLockCode
        {
            get
            {
                return this.fixationLockCodeField;
            }
            set
            {
                this.fixationLockCodeField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime StartDateLoadSpace
        {
            get
            {
                return this.startDateLoadSpaceField;
            }
            set
            {
                this.startDateLoadSpaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime StartTimeLoadSpace
        {
            get
            {
                return this.startTimeLoadSpaceField;
            }
            set
            {
                this.startTimeLoadSpaceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "date")]
        public System.DateTime CombinationPlanningEndDate
        {
            get
            {
                return this.combinationPlanningEndDateField;
            }
            set
            {
                this.combinationPlanningEndDateField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime CombinationPlanningEndTime
        {
            get
            {
                return this.combinationPlanningEndTimeField;
            }
            set
            {
                this.combinationPlanningEndTimeField = value;
            }
        }

        /// <remarks/>
        public string CombinationPlanningWorktime
        {
            get
            {
                return this.combinationPlanningWorktimeField;
            }
            set
            {
                this.combinationPlanningWorktimeField = value;
            }
        }

        /// <remarks/>
        public string CombinationPlanningDrivingTime
        {
            get
            {
                return this.combinationPlanningDrivingTimeField;
            }
            set
            {
                this.combinationPlanningDrivingTimeField = value;
            }
        }

        /// <remarks/>
        public string CombinationPlanningWaitingTime
        {
            get
            {
                return this.combinationPlanningWaitingTimeField;
            }
            set
            {
                this.combinationPlanningWaitingTimeField = value;
            }
        }

        /// <remarks/>
        public string CombinationPlanningDistance
        {
            get
            {
                return this.combinationPlanningDistanceField;
            }
            set
            {
                this.combinationPlanningDistanceField = value;
            }
        }

        /// <remarks/>
        public string CombinationPlanningTotalCosts
        {
            get
            {
                return this.combinationPlanningTotalCostsField;
            }
            set
            {
                this.combinationPlanningTotalCostsField = value;
            }
        }

        /// <remarks/>
        public string NumberOfRoutes
        {
            get
            {
                return this.numberOfRoutesField;
            }
            set
            {
                this.numberOfRoutesField = value;
            }
        }

        /// <remarks/>
        public string ViolationCode
        {
            get
            {
                return this.violationCodeField;
            }
            set
            {
                this.violationCodeField = value;
            }
        }

        /// <remarks/>
        public string ReferenceLEOPlanScenario
        {
            get
            {
                return this.referenceLEOPlanScenarioField;
            }
            set
            {
                this.referenceLEOPlanScenarioField = value;
            }
        }

        /// <remarks/>
        public string ReferenceRouteID
        {
            get
            {
                return this.referenceRouteIDField;
            }
            set
            {
                this.referenceRouteIDField = value;
            }
        }

        /// <remarks/>
        public string ReferenceRouteSequenceNumber
        {
            get
            {
                return this.referenceRouteSequenceNumberField;
            }
            set
            {
                this.referenceRouteSequenceNumberField = value;
            }
        }

        /// <remarks/>
        public string LicensePlateNumber
        {
            get
            {
                return this.licensePlateNumberField;
            }
            set
            {
                this.licensePlateNumberField = value;
            }
        }

        /// <remarks/>
        public string LEOFicticiousCosts
        {
            get
            {
                return this.lEOFicticiousCostsField;
            }
            set
            {
                this.lEOFicticiousCostsField = value;
            }
        }

        /// <remarks/>
        public string HelperAssigned
        {
            get
            {
                return this.helperAssignedField;
            }
            set
            {
                this.helperAssignedField = value;
            }
        }

        /// <remarks/>
        public string VehicleName
        {
            get
            {
                return this.vehicleNameField;
            }
            set
            {
                this.vehicleNameField = value;
            }
        }

        /// <remarks/>
        public string EquipmentNumber
        {
            get
            {
                return this.equipmentNumberField;
            }
            set
            {
                this.equipmentNumberField = value;
            }
        }

        /// <remarks/>
        public string ResourcesSize1
        {
            get
            {
                return this.resourcesSize1Field;
            }
            set
            {
                this.resourcesSize1Field = value;
            }
        }

        /// <remarks/>
        public string ResourcesSize2
        {
            get
            {
                return this.resourcesSize2Field;
            }
            set
            {
                this.resourcesSize2Field = value;
            }
        }

        /// <remarks/>
        public string ResourcesSize3
        {
            get
            {
                return this.resourcesSize3Field;
            }
            set
            {
                this.resourcesSize3Field = value;
            }
        }

        /// <remarks/>
        public string LEODepotID
        {
            get
            {
                return this.lEODepotIDField;
            }
            set
            {
                this.lEODepotIDField = value;
            }
        }

        /// <remarks/>
        public string TransportationPlanningPostring
        {
            get
            {
                return this.transportationPlanningPostringField;
            }
            set
            {
                this.transportationPlanningPostringField = value;
            }
        }

        /// <remarks/>
        public string ExplanationCode
        {
            get
            {
                return this.explanationCodeField;
            }
            set
            {
                this.explanationCodeField = value;
            }
        }

        /// <remarks/>
        public string VehicleType
        {
            get
            {
                return this.vehicleTypeField;
            }
            set
            {
                this.vehicleTypeField = value;
            }
        }

        /// <remarks/>
        public string VehicleDescription
        {
            get
            {
                return this.vehicleDescriptionField;
            }
            set
            {
                this.vehicleDescriptionField = value;
            }
        }

        /// <remarks/>
        public string ReturnToDepot
        {
            get
            {
                return this.returnToDepotField;
            }
            set
            {
                this.returnToDepotField = value;
            }
        }

        /// <remarks/>
        public string DistributionMode
        {
            get
            {
                return this.distributionModeField;
            }
            set
            {
                this.distributionModeField = value;
            }
        }

        /// <remarks/>
        public string ForklitOnBoard
        {
            get
            {
                return this.forklitOnBoardField;
            }
            set
            {
                this.forklitOnBoardField = value;
            }
        }

        /// <remarks/>
        public string FixTimeBeforeTrip
        {
            get
            {
                return this.fixTimeBeforeTripField;
            }
            set
            {
                this.fixTimeBeforeTripField = value;
            }
        }

        /// <remarks/>
        public string FixReloadTime
        {
            get
            {
                return this.fixReloadTimeField;
            }
            set
            {
                this.fixReloadTimeField = value;
            }
        }

        /// <remarks/>
        public string FixTimeAfterTrip
        {
            get
            {
                return this.fixTimeAfterTripField;
            }
            set
            {
                this.fixTimeAfterTripField = value;
            }
        }

        /// <remarks/>
        public string FixedTimeUnit
        {
            get
            {
                return this.fixedTimeUnitField;
            }
            set
            {
                this.fixedTimeUnitField = value;
            }
        }

        /// <remarks/>
        public string FictiveClusterCosts
        {
            get
            {
                return this.fictiveClusterCostsField;
            }
            set
            {
                this.fictiveClusterCostsField = value;
            }
        }

        /// <remarks/>
        public string FictiveLocationCosts
        {
            get
            {
                return this.fictiveLocationCostsField;
            }
            set
            {
                this.fictiveLocationCostsField = value;
            }
        }

        /// <remarks/>
        public string FictiveMinimumCases
        {
            get
            {
                return this.fictiveMinimumCasesField;
            }
            set
            {
                this.fictiveMinimumCasesField = value;
            }
        }

        /// <remarks/>
        public string TotalWeight1
        {
            get
            {
                return this.totalWeight1Field;
            }
            set
            {
                this.totalWeight1Field = value;
            }
        }

        /// <remarks/>
        public string DriverProductivity
        {
            get
            {
                return this.driverProductivityField;
            }
            set
            {
                this.driverProductivityField = value;
            }
        }

        /// <remarks/>
        public string CO2RackType
        {
            get
            {
                return this.cO2RackTypeField;
            }
            set
            {
                this.cO2RackTypeField = value;
            }
        }

        /// <remarks/>
        public string ThresholdMiniPallets
        {
            get
            {
                return this.thresholdMiniPalletsField;
            }
            set
            {
                this.thresholdMiniPalletsField = value;
            }
        }

        /// <remarks/>
        public string ThresholdSuperMiniPallets
        {
            get
            {
                return this.thresholdSuperMiniPalletsField;
            }
            set
            {
                this.thresholdSuperMiniPalletsField = value;
            }
        }

        /// <remarks/>
        public string DistributionMode2
        {
            get
            {
                return this.distributionMode2Field;
            }
            set
            {
                this.distributionMode2Field = value;
            }
        }

        /// <remarks/>
        public string DurationBreakTimeNew
        {
            get
            {
                return this.durationBreakTimeNewField;
            }
            set
            {
                this.durationBreakTimeNewField = value;
            }
        }
    }

    /// <remarks/>
    [System.SerializableAttribute()]
    [System.ComponentModel.DesignerCategoryAttribute("code")]
    [System.Xml.Serialization.XmlTypeAttribute(AnonymousType = true)]
    [System.Xml.Serialization.XmlRootAttribute(Namespace = "", IsNullable = false)]
    public partial class Item
    {

        private string planIDField;

        private string scenarioIDField;

        private string tourIDField;

        private string tourSequenceField;

        private string routeItemField;

        private string taskIDField;

        private string depotIDField;

        private string lockcodeField;

        private string mapReferenceIdentifierField;

        private string arrivalDateStopField;

        private System.DateTime arrivalTimeField;

        private string realisedArrivalDateStopField;

        private System.DateTime realisedArrivalTimeField;

        private string departureDateStopField;

        private System.DateTime departureTimeField;

        private string realisedDepartureDateStopField;

        private System.DateTime realisedDepartureTimeField;

        private string waitTimeStopField;

        private string driveTimeField;

        private string workTimeStopField;

        private string distanceField;

        private bool distanceFieldSpecified;

        private string totalUnloadWeightField;

        private bool totalUnloadWeightFieldSpecified;

        private string totalUnloadQtySize2Field;

        private bool totalUnloadQtySize2FieldSpecified;

        private string totalUnloadQtySize3Field;

        private bool totalUnloadQtySize3FieldSpecified;

        private string violationCodeField;

        private string name1Field;

        private string houseNumberField;

        private string postalCodeField;

        private string cityField;

        private string customerNumberField;

        private bool customerNumberFieldSpecified;

        private string size1DField;

        private bool size1DFieldSpecified;

        private string size2DField;

        private bool size2DFieldSpecified;

        private string size3DField;

        private bool size3DFieldSpecified;

        private System.DateTime startTimeWindow1Field;

        private bool startTimeWindow1FieldSpecified;

        private System.DateTime endTimeWindow1Field;

        private bool endTimeWindow1FieldSpecified;

        private string visitListPositionField;

        private string additionalCustomerNumberField;

        private bool additionalCustomerNumberFieldSpecified;

        private string detachField;

        private bool detachFieldSpecified;

        private string attachField;

        private bool attachFieldSpecified;

        private string breakDurationField;

        private string pauseStopField;

        private string pauseStopXField;

        private string pauseStopYField;

        private string num1Field;

        private string deliveryCDPSField;

        private bool deliveryCDPSFieldSpecified;

        /// <remarks/>
        public string PlanID
        {
            get
            {
                return this.planIDField;
            }
            set
            {
                this.planIDField = value;
            }
        }

        /// <remarks/>
        public string ScenarioID
        {
            get
            {
                return this.scenarioIDField;
            }
            set
            {
                this.scenarioIDField = value;
            }
        }

        /// <remarks/>
        public string TourID
        {
            get
            {
                return this.tourIDField;
            }
            set
            {
                this.tourIDField = value;
            }
        }

        /// <remarks/>
        public string TourSequence
        {
            get
            {
                return this.tourSequenceField;
            }
            set
            {
                this.tourSequenceField = value;
            }
        }

        /// <remarks/>
        public string RouteItem
        {
            get
            {
                return this.routeItemField;
            }
            set
            {
                this.routeItemField = value;
            }
        }

        /// <remarks/>
        public string TaskID
        {
            get
            {
                return this.taskIDField;
            }
            set
            {
                this.taskIDField = value;
            }
        }

        /// <remarks/>
        public string DepotID
        {
            get
            {
                return this.depotIDField;
            }
            set
            {
                this.depotIDField = value;
            }
        }

        /// <remarks/>
        public string Lockcode
        {
            get
            {
                return this.lockcodeField;
            }
            set
            {
                this.lockcodeField = value;
            }
        }

        /// <remarks/>
        public string MapReferenceIdentifier
        {
            get
            {
                return this.mapReferenceIdentifierField;
            }
            set
            {
                this.mapReferenceIdentifierField = value;
            }
        }

        /// <remarks/>
        public string ArrivalDateStop
        {
            get
            {
                return this.arrivalDateStopField;
            }
            set
            {
                this.arrivalDateStopField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime ArrivalTime
        {
            get
            {
                return this.arrivalTimeField;
            }
            set
            {
                this.arrivalTimeField = value;
            }
        }

        /// <remarks/>
        public string RealisedArrivalDateStop
        {
            get
            {
                return this.realisedArrivalDateStopField;
            }
            set
            {
                this.realisedArrivalDateStopField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime RealisedArrivalTime
        {
            get
            {
                return this.realisedArrivalTimeField;
            }
            set
            {
                this.realisedArrivalTimeField = value;
            }
        }

        /// <remarks/>
        public string DepartureDateStop
        {
            get
            {
                return this.departureDateStopField;
            }
            set
            {
                this.departureDateStopField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime DepartureTime
        {
            get
            {
                return this.departureTimeField;
            }
            set
            {
                this.departureTimeField = value;
            }
        }

        /// <remarks/>
        public string RealisedDepartureDateStop
        {
            get
            {
                return this.realisedDepartureDateStopField;
            }
            set
            {
                this.realisedDepartureDateStopField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime RealisedDepartureTime
        {
            get
            {
                return this.realisedDepartureTimeField;
            }
            set
            {
                this.realisedDepartureTimeField = value;
            }
        }

        /// <remarks/>
        public string WaitTimeStop
        {
            get
            {
                return this.waitTimeStopField;
            }
            set
            {
                this.waitTimeStopField = value;
            }
        }

        /// <remarks/>
        public string DriveTime
        {
            get
            {
                return this.driveTimeField;
            }
            set
            {
                this.driveTimeField = value;
            }
        }

        /// <remarks/>
        public string WorkTimeStop
        {
            get
            {
                return this.workTimeStopField;
            }
            set
            {
                this.workTimeStopField = value;
            }
        }

        /// <remarks/>
        public string Distance
        {
            get
            {
                return this.distanceField;
            }
            set
            {
                this.distanceField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DistanceSpecified
        {
            get
            {
                return this.distanceFieldSpecified;
            }
            set
            {
                this.distanceFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string TotalUnloadWeight
        {
            get
            {
                return this.totalUnloadWeightField;
            }
            set
            {
                this.totalUnloadWeightField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalUnloadWeightSpecified
        {
            get
            {
                return this.totalUnloadWeightFieldSpecified;
            }
            set
            {
                this.totalUnloadWeightFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string TotalUnloadQtySize2
        {
            get
            {
                return this.totalUnloadQtySize2Field;
            }
            set
            {
                this.totalUnloadQtySize2Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalUnloadQtySize2Specified
        {
            get
            {
                return this.totalUnloadQtySize2FieldSpecified;
            }
            set
            {
                this.totalUnloadQtySize2FieldSpecified = value;
            }
        }

        /// <remarks/>
        public string TotalUnloadQtySize3
        {
            get
            {
                return this.totalUnloadQtySize3Field;
            }
            set
            {
                this.totalUnloadQtySize3Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool TotalUnloadQtySize3Specified
        {
            get
            {
                return this.totalUnloadQtySize3FieldSpecified;
            }
            set
            {
                this.totalUnloadQtySize3FieldSpecified = value;
            }
        }

        /// <remarks/>
        public string ViolationCode
        {
            get
            {
                return this.violationCodeField;
            }
            set
            {
                this.violationCodeField = value;
            }
        }

        /// <remarks/>
        public string Name1
        {
            get
            {
                return this.name1Field;
            }
            set
            {
                this.name1Field = value;
            }
        }

        /// <remarks/>
        public string HouseNumber
        {
            get
            {
                return this.houseNumberField;
            }
            set
            {
                this.houseNumberField = value;
            }
        }

        /// <remarks/>
        public string PostalCode
        {
            get
            {
                return this.postalCodeField;
            }
            set
            {
                this.postalCodeField = value;
            }
        }

        /// <remarks/>
        public string City
        {
            get
            {
                return this.cityField;
            }
            set
            {
                this.cityField = value;
            }
        }

        /// <remarks/>
        public string CustomerNumber
        {
            get
            {
                return this.customerNumberField;
            }
            set
            {
                this.customerNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool CustomerNumberSpecified
        {
            get
            {
                return this.customerNumberFieldSpecified;
            }
            set
            {
                this.customerNumberFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Size1D
        {
            get
            {
                return this.size1DField;
            }
            set
            {
                this.size1DField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Size1DSpecified
        {
            get
            {
                return this.size1DFieldSpecified;
            }
            set
            {
                this.size1DFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Size2D
        {
            get
            {
                return this.size2DField;
            }
            set
            {
                this.size2DField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Size2DSpecified
        {
            get
            {
                return this.size2DFieldSpecified;
            }
            set
            {
                this.size2DFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Size3D
        {
            get
            {
                return this.size3DField;
            }
            set
            {
                this.size3DField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool Size3DSpecified
        {
            get
            {
                return this.size3DFieldSpecified;
            }
            set
            {
                this.size3DFieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime StartTimeWindow1
        {
            get
            {
                return this.startTimeWindow1Field;
            }
            set
            {
                this.startTimeWindow1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool StartTimeWindow1Specified
        {
            get
            {
                return this.startTimeWindow1FieldSpecified;
            }
            set
            {
                this.startTimeWindow1FieldSpecified = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlElementAttribute(DataType = "time")]
        public System.DateTime EndTimeWindow1
        {
            get
            {
                return this.endTimeWindow1Field;
            }
            set
            {
                this.endTimeWindow1Field = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool EndTimeWindow1Specified
        {
            get
            {
                return this.endTimeWindow1FieldSpecified;
            }
            set
            {
                this.endTimeWindow1FieldSpecified = value;
            }
        }

        /// <remarks/>
        public string VisitListPosition
        {
            get
            {
                return this.visitListPositionField;
            }
            set
            {
                this.visitListPositionField = value;
            }
        }

        /// <remarks/>
        public string AdditionalCustomerNumber
        {
            get
            {
                return this.additionalCustomerNumberField;
            }
            set
            {
                this.additionalCustomerNumberField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AdditionalCustomerNumberSpecified
        {
            get
            {
                return this.additionalCustomerNumberFieldSpecified;
            }
            set
            {
                this.additionalCustomerNumberFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Detach
        {
            get
            {
                return this.detachField;
            }
            set
            {
                this.detachField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DetachSpecified
        {
            get
            {
                return this.detachFieldSpecified;
            }
            set
            {
                this.detachFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string Attach
        {
            get
            {
                return this.attachField;
            }
            set
            {
                this.attachField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool AttachSpecified
        {
            get
            {
                return this.attachFieldSpecified;
            }
            set
            {
                this.attachFieldSpecified = value;
            }
        }

        /// <remarks/>
        public string BreakDuration
        {
            get
            {
                return this.breakDurationField;
            }
            set
            {
                this.breakDurationField = value;
            }
        }

        /// <remarks/>
        public string PauseStop
        {
            get
            {
                return this.pauseStopField;
            }
            set
            {
                this.pauseStopField = value;
            }
        }

        /// <remarks/>
        public string PauseStopX
        {
            get
            {
                return this.pauseStopXField;
            }
            set
            {
                this.pauseStopXField = value;
            }
        }

        /// <remarks/>
        public string PauseStopY
        {
            get
            {
                return this.pauseStopYField;
            }
            set
            {
                this.pauseStopYField = value;
            }
        }

        /// <remarks/>
        public string Num1
        {
            get
            {
                return this.num1Field;
            }
            set
            {
                this.num1Field = value;
            }
        }

        /// <remarks/>
        public string DeliveryCDPS
        {
            get
            {
                return this.deliveryCDPSField;
            }
            set
            {
                this.deliveryCDPSField = value;
            }
        }

        /// <remarks/>
        [System.Xml.Serialization.XmlIgnoreAttribute()]
        public bool DeliveryCDPSSpecified
        {
            get
            {
                return this.deliveryCDPSFieldSpecified;
            }
            set
            {
                this.deliveryCDPSFieldSpecified = value;
            }
        }
    }
}
