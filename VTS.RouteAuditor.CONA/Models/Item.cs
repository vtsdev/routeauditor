﻿using System;
using System.Collections.Generic;
using System.Text;
using VTS.RouteAuditor.Common.Interfaces;

namespace VTS.RouteAuditor.CONA.Models
{
    public partial class Item : IPlannedStop
    {
        public long Id { get; set; }

        public long RoutePlanId { get; set; }

        public string TaskId => this.TaskID;

        //public string CustomerNumber => throw new NotImplementedException();

        //public TimeSpan ArrivalTime => throw new NotImplementedException();

        //public TimeSpan DepartureTime => throw new NotImplementedException();

        public int Cases => int.TryParse(Size2D, out var cases) ? cases * 100 : 0;

        public int Cubes => int.TryParse(Size3D, out var cubes) ? cubes : 0;
    }
}
