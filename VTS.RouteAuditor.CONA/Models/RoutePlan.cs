﻿using System;
using System.Collections.Generic;
using System.Text;
using VTS.RouteAuditor.Common.Interfaces;

namespace VTS.RouteAuditor.CONA.Models
{
    public partial class RoutePlan : IRoutePlan
    {
        public long Id { get; set; }

        public string RouteIdentifier => PlanHeader.LEOFixedRoute;

        public DateTime RouteDate => PlanHeader.StartDateRoute;

        public string SourceFile { get; set; }

        public string TourId => PlanHeader.TourID;

        public DateTime StartDateTime => PlanHeader.StartTimeRoute;

        public DateTime EndDateTime => PlanHeader.EndTimeRoute;

        private TimeSpan? _driveTime;
        public TimeSpan DriveTime
        {
            get
            {
                if (!_driveTime.HasValue)
                    _driveTime = ParseTimespan(PlanHeader.TotalDriveTimeRoute);

                return _driveTime.Value;
            }
        }

        private TimeSpan? _waitTime;
        public TimeSpan WaitTime
        {
            get
            {
                if (!_waitTime.HasValue)
                    _waitTime = ParseTimespan(PlanHeader.WaitTimeRoute);

                return _waitTime.Value;
            }
        }

        private double? _distance;
        public double Distance
        {
            get
            {
                if (!_distance.HasValue)
                    _distance = double.TryParse(PlanHeader.TotalDistanceRoute, out var distance) ? distance : 0;

                return _distance.Value;
            }
        }

        public IEnumerable<IPlannedStop> PlannedStops => Items;


        private TimeSpan ParseTimespan(string input)
        {
            var match = System.Text.RegularExpressions.Regex.Match(input.Trim(), @"^(?<Hours>[0-9]{3})(?<Minutes>[0-9]{2})$");
            if (match.Success && 
                int.TryParse(match.Groups["Hours"].Value, out var hours) &&
                int.TryParse(match.Groups["Minutes"].Value, out var minutes))
            {
                return new TimeSpan(hours, minutes, 0);
            }

            return TimeSpan.Zero;
        }
    }
}
