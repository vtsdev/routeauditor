﻿using CsvHelper.Configuration.Attributes;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using VTS.RouteAuditor.Common.Interfaces;

namespace VTS.RouteAuditor.CONA.Models
{
    public class Shipment : IShipment
    {
        private static Regex _driverNumberExpression = new Regex(@"^08990(?<DriverIdentifier>[\d]{5})$", RegexOptions.Compiled);
        #region CSV Columns
        [Name("Shipment Number")]
        public string ShipmentNumber { get; set; }
        [Name("Created On")]
        public DateTime CreatedOnDate { get; set; }
        [Name("Time")]
        public DateTime CreatedOnTime { get; set; }
        [Name("Changed By")]
        public string ChangedBy { get; set; }
        [Name("Date Changed")]
        public DateTime ChangedOnDate { get; set; }
        [Name("Time of Change")]
        public DateTime ChangedOnTime { get; set; }
        [Name("Driver Number")]
        public string DriverNumber { get; set; }
        [Name("Vehicle")]
        public string Vehicle { get; set; }
        [Name("Trailer")]
        public string Trailer { get; set; }
        [Name("LEO Plan Number")]
        public string LEOPlanNumber { get; set; }
        [Name("TPP")]
        public string TPP { get; set; }
        [Name("Shipping Point")]
        public string ShippingPoint { get; set; }
        [Name("Distribution Mode")]
        public string DistributionMode { get; set; }
        [Name("Master Route")]
        public string MasterRoute { get; set; }
        [Name("Route Number")]
        public string RouteNumber { get; set; }
        [Name("Fixed Route")]
        public string FixedRoute { get; set; }
        #endregion

        public DateTime RouteDate { get; private set; }

        public string RouteIdentifier => FixedRoute;

        public string TourIdentifier => RouteNumber;

        public string DriverIdentifier => _driverNumberExpression.IsMatch(DriverNumber) ? _driverNumberExpression.Match(DriverNumber).Groups["DriverIdentifier"].Value : DriverNumber;

        public string VehicleIdentifier => Vehicle;

        public string ShipmentIdentifier => ShipmentNumber;

        public void SetRouteDate(DateTime routeDate)
        {
            RouteDate = routeDate;
        }
    }
}
